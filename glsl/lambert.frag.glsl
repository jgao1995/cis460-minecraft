#version 150
// ^ Change this to version 130 if you have compatibility issues

// This is a fragment shader. If you've opened this file first, please
// open and read lambert.vert.glsl before reading on.
// Unlike the vertex shader, the fragment shader actually does compute
// the shading of geometry. For every pixel in your program's output
// screen, the fragment shader is run for every bit of geometry that
// particular pixel overlaps. By implicitly interpolating the position
// data passed into the fragment shader by the vertex shader, the fragment shader
// can compute what color to apply to its pixel based on things like vertex
// position, light position, and vertex color.

uniform vec4 u_Color; // The color with which to render this instance of geometry.
uniform sampler2D textureSampler;
uniform sampler2D normalSampler;
uniform int u_Time;
uniform int u_Night;

// These are the interpolated values out of the rasterizer, so you can't know
// their specific values without knowing the vertices that contributed to them
in vec4 fs_Nor;
in vec4 fs_LightVec;
in vec4 fs_Col;
in vec2 fs_UV;

out vec4 out_Col; // This is the final output color that you will see on your
                  // screen for the pixel that is currently being processed.

void main()
{
    // Material base color (before shading)
        vec4 diffuseColor = fs_Col;

        // Calculate the diffuse term for Lambert shading
        float diffuseTerm = dot(normalize(fs_Nor), normalize(fs_LightVec));
        // Avoid negative lighting values
        diffuseTerm = clamp(diffuseTerm, 0, 1);

        float ambientTerm = 0.2;


        float lightIntensity = diffuseTerm + ambientTerm;   //Add a small float value to the color multiplier
                                                            //to simulate ambient lighting. This ensures that faces that are not
                                                            //lit by our point light are not completely black.
        if (u_Night == 1) {
            lightIntensity = lightIntensity * 0.5;
        }
        // Compute final shaded color
        //out_Col = vec4(diffuseColor.rgb * lightIntensity, diffuseColor.a);
//        fs_UV[0] = fs_UV[0] + ((u_Time % 16) / 16);
//        fs_UV[1] = fs_UV[1] + ((u_Time % 16) / 16);
        //vec2 new_UV = vec2(fs_UV[0] + u_Time * 0.00390625, fs_UV[1]);
        //vec2 new_UV = vec2(fs_UV[0], fs_UV[1]);
        //out_Col = texture(textureSampler, new_UV);
        if (fs_UV[0] > 13.0/16 && fs_UV[0] < 14.0/16 && fs_UV[1] > 14.0/16 && fs_UV[1] < 15.0/16 ||
            fs_UV[0] > 13.0/16 && fs_UV[0] < 14.0/16 && fs_UV[1] > 12.0/16 && fs_UV[1] < 13.0/16) {
            // this is the lava boundary
            vec2 new_UV = vec2(fs_UV[0] + u_Time * 0.00390625, fs_UV[1]);
            out_Col = texture(textureSampler, new_UV) * lightIntensity;
        }
        else {
            out_Col = texture(textureSampler, fs_UV) * lightIntensity;
        }
}
