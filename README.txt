Joseph Gao, Suzanne Knop, Zen Luo
Mini Minecraft README
CIS 460
Adam Mally

================
Important Notes!
================

1. For grading purposes, I have included all my .wav files and threw them into the appropriate .qrc files and folders. This means you don’t have to do anything special to hear the sound. 

HOWEVER, SOIL would not play nice with the .qrc resource path, and thus YOU MUST MANUALLY PROVIDE THE CORRECT, ENTIRE PATH FOR THE TEXTURE FILE OR EVERYTHING WILL BE BLACK. GO TO MYGL.CPP AND ADD IN THE CORRECT PATH OF THE SOIL LOADING FUNCTION!!!!

I REPEAT

*EVERYTHING WILL BE BLACK (NO TEXTURES) IF YOU DO NOT PROVIDE THE CORRECT FULL TEXTURE FILE PATH FOR SOIL!!!! (see our commented out paths for reference in mygl.cpp, around lines 55). 

2. Sometimes sound does not work as expected (stepping on a river does not allow it to play the sound. I promise it works, go find a another river!

3. Weather kicks in after a certain amount of time. The day cycle goes for a few minecraft hours (like 10 secs) and then the weather starts. 

4. Please make sure SOIL is set up correctly as well. You will have to make sure the path is correct in mygl, and if all else fails, please try doing this on a mac (we all developed with mac only). 


=================
BREAKDOWN OF WORK
=================

* MILESTONE 1 *
Zen - Perlin noise landscape generation
Joseph - Efficient face rendering and interleaved VBOs.
Suzanne - Camera controls and movement, adding/removing blocks 

* MILESTONE 2 * 
Zen - Caves, Tunnels, Underground stuff
Joseph - Texturing, Animation of LAVA and WATER, Blinn-Phong shading, Normal maps
Suzanne - L-system rivers, sandbox view

* FINAL MILESTONE *
Zen - Biomes and NPC/AI (130 PTS total)
Joseph - Sound and Day/Night cycle (45 PTS total)
Suzanne - Weather (50 PTS total)



