QT += core widgets
QT += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets multimedia
QT += multimedia
TARGET = 277
TEMPLATE = app
CONFIG += console
CONFIG += c++11
CONFIG += warn_on
CONFIG += debug
QMAKE_CXXFLAGS += -std=c++11
INCLUDEPATH += include

include(src/src.pri)

FORMS += forms/mainwindow.ui \
    forms/cameracontrolshelp.ui

RESOURCES += glsl.qrc \
    images.qrc \
    sounds.qrc

*-clang*|*-g++* {
    message("Enabling additional warnings")
    CONFIG -= warn_on
    QMAKE_CXXFLAGS += -Wall -Wextra -pedantic -Winit-self
    QMAKE_CXXFLAGS += -Wno-strict-aliasing
    QMAKE_CXXFLAGS += -fno-omit-frame-pointer
}
linux-clang*|linux-g++*|macx-clang*|macx-g++* {
    message("Enabling stack protector")
    QMAKE_CXXFLAGS += -fstack-protector-all
}

# FOR LINUX & MAC USERS INTERESTED IN ADDITIONAL BUILD TOOLS
# ----------------------------------------------------------
# This conditional exists to enable Address Sanitizer (ASAN) during
# the automated build. ASAN is a compiled-in tool which checks for
# memory errors (like Valgrind). You may enable it for yourself;
# check the hidden `.build.sh` file for info. But be aware: ASAN may
# trigger a lot of false-positive leak warnings for the Qt libraries.
# (See `.run.sh` for how to disable leak checking.)
address_sanitizer {
    message("Enabling Address Sanitizer")
    QMAKE_CXXFLAGS += -fsanitize=address
    QMAKE_LFLAGS += -fsanitize=address
}

HEADERS +=

SOURCES +=

QMAKE_LFLAGS += -F /System/Library/Frameworks/CoreFoundation.framework/
LIBS += -framework CoreFoundation

macx: LIBS += -L$$PWD/../../../../../Downloads/libSOIL-master/ -lSOIL

INCLUDEPATH += $$PWD/../../../../../Downloads/libSOIL-master
DEPENDPATH += $$PWD/../../../../../Downloads/libSOIL-master

macx: PRE_TARGETDEPS += $$PWD/../../../../../Downloads/libSOIL-master/libSOIL.a
LIBS += -framework OpenGL




win32:CONFIG(release, debug|release): LIBS += -L$$PWD/'../Simple OpenGL Image Library/lib/release/' -lSOIL
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/'../Simple OpenGL Image Library/lib/debug/' -lSOIL
else:unix:!macx: LIBS += -L$$PWD/'../Simple OpenGL Image Library/lib/' -lSOIL

INCLUDEPATH += $$PWD/'../Simple OpenGL Image Library'
DEPENDPATH += $$PWD/'../Simple OpenGL Image Library'

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/'../Simple OpenGL Image Library/lib/release/libSOIL.a'
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/'../Simple OpenGL Image Library/lib/debug/libSOIL.a'
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/'../Simple OpenGL Image Library/lib/release/SOIL.lib'
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/'../Simple OpenGL Image Library/lib/debug/SOIL.lib'
else:unix:!macx: PRE_TARGETDEPS += $$PWD/'../Simple OpenGL Image Library/lib/libSOIL.a'
