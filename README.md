# Mini Minecraft #

Final Project for CIS 460/560.

### Week 1 Milestone ###

1. **Procedural Terrain** implemented by Zen

2. **Efficient Terrain Rendering** implemented by Joseph

3. **Terrain Collisions and Physics** implemented by Suzanne

### Week 2 Milestone ###

1. **Procedural Cave Generation** implemented by Zen

2. **Texturing and Texture Animation** implemented by Joseph

3. **L-System Rivers and Sandbox Mode Camera** implemented by Suzanne

### Week 3 Milestone ### 
1. **Biomes and NPC/AI** implemented by Zen
2. **Day/Night Cycles and Sound** implemented by Joseph
3. **Weather** implemented by Suzanne