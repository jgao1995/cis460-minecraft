#include "mygl.h"
#include <la.h>

#include <iostream>
#include <QApplication>
#include <QKeyEvent>
#include <QFileInfo>

#include "scene/chunk.h"
#include <stdlib.h>
#include <time.h>

MyGL::MyGL(QWidget *parent)
    : GLWidget277(parent),
      geom_cube(this), geom_plus(this),
      prog_lambert(this), prog_flat(this),
      //c(this),
      gl_camera(),
      step_sound(":/sounds/step.wav"),
      water_sound(":/sounds/water.wav"),
      wind_sound(":/sounds/wind.wav"),
      misty_mountains(":/sounds/recorder.wav"),
      movingObjs(std::vector<MovingObj*>()),
      geom_precip(this),
      weatherObjs(std::vector<Weather*>())
{
    // Connect the timer to a function so that when the timer ticks the function is executed
    connect(&timer, SIGNAL(timeout()), this, SLOT(timerUpdate()));
    // Tell the timer to redraw 60 times per second
    timer.start(16);
    setFocusPolicy(Qt::ClickFocus);
    updatingClock = 0;
    srand (time(NULL));
}

MyGL::~MyGL()
{
    makeCurrent();
    glDeleteVertexArrays(1, &vao);
    geom_cube.destroy();
    geom_precip.destroy();
}

void MyGL::initializeGL()
{
    // Create an OpenGL context using Qt's QOpenGLFunctions_3_2_Core class
    // If you were programming in a non-Qt context you might use GLEW (GL Extension Wrangler)instead
    initializeOpenGLFunctions();
    // Print out some information about the current OpenGL context
    debugContextVersion();

    // ******* GRADER PLEASE ADD YOUR TEXTURE FILE TO SOIL HERE *******

    //Suzanne
    //image = SOIL_load_image("/Users/suzanneknop/Documents/Penn/Classes/CIS 460/cis460-minecraft/minecraft_textures_all.png", &text_width, &text_height, 0, SOIL_LOAD_RGB);
    //    image = SOIL_load_image("/Users/suzanneknop/Documents/Penn/Classes/CIS 460/cis460-minecraft/minecraft_textures_all.png", &text_width, &text_height, 0, SOIL_LOAD_RGB);

    //Zen
    image = SOIL_load_image("/Users/zen/Documents/Developer/Penn Engineering/CIS 460/cis460-minecraft/minecraft_textures_all.png", &text_width, &text_height, 0, SOIL_LOAD_RGB);

    //Joe
    //    image = SOIL_load_image("/Users/josephgao/Developer/qt/cis560/final-project/cis460-minecraft/minecraft_textures_all.png", &text_width, &text_height, 0, SOIL_LOAD_RGB);

    printf("SOIL results: '%s'\n", SOIL_last_result());

    //    normal_map = SOIL_load_image("/Users/josephgao/Developer/qt/cis560/final-project/cis460-minecraft/minecraft_textures_all.png", &text_width, &text_height, 0, SOIL_LOAD_RGB);
    //    printf("SOIL results: '%s'\n", SOIL_last_result());

    // Set a few settings/modes in OpenGL rendering
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
    // Set the size with which points should be rendered
    glPointSize(5);
    // Set the color with which the screen is filled at the start of each render call.
    glClearColor(0.37f, 0.74f, 1.0f, 1);
    //glClearColor(0.098f, 0.098f, 0.439f, 1);

    printGLErrorLog();

    // Create a Vertex Attribute Object
    glGenVertexArrays(1, &vao);

    //Create the instance of Cube
    geom_cube.create();
    geom_plus.create();
    geom_precip.create();

    //c.create();

    // Create and set up the diffuse shader
    prog_lambert.create(":/glsl/lambert.vert.glsl", ":/glsl/lambert.frag.glsl");
    // Create and set up the flat lighting shader
    prog_flat.create(":/glsl/flat.vert.glsl", ":/glsl/flat.frag.glsl");

    // Set a color with which to draw geometry since you won't have one
    // defined until you implement the Node classes.
    // This makes your geometry render green.
    prog_lambert.setGeometryColor(glm::vec4(0,1,0,1));

    // We have to have a VAO bound in OpenGL 3.2 Core. But if we're not
    // using multiple VAOs, we can just bind one once.
    //    vao.bind();
    glBindVertexArray(vao);

    glGenTextures(1, &textureHandle);

    for(int i = 0; i < NUM_OFSHEEPS; ++i){
        movingObjs.push_back(new MovingObj(gl_camera.eye.x + 10, gl_camera.eye.y, gl_camera.eye.z + 10));
    }


    //Pass the MyGL handler to the scene to create chunks
    scene.setMyGl(this);
    scene.CreateTestScene();
    emit signal_sendSeed(scene.seed);
}

void MyGL::populateWeather(BlockType b) {
    weatherObjs.clear();

    int weather_x = gl_camera.eye.x;
    int weather_y = 80;
    int weather_z = gl_camera.eye.z;
    for(int i = 0; i < 7; i++){
        for(int j = 0; j < 2; j++){
            for(int k = 0; k < 7; k++){
                weatherObjs.push_back(new Weather(weather_x + i + (rand() % 70), weather_y + j + (rand() % 70), weather_z + k + (rand() % 70), b));
            }
        }
    }

}

void initPlayerPos(Scene* scene, Camera* camera)
{
    glm::vec3 eye = camera->eye;
    int x = eye.x;
    int y = eye.y;
    int z = eye.z;
    Chunk* chunk = scene->terrinChunkMap.at(scene->curboudingBox);

    while (chunk->cubeAt(x, y ,z) || chunk->cubeAt(x, y - 1 ,z))
    {
        camera->TranslateWorldY(1);
        y++;
    }
}

void MyGL::resizeGL(int w, int h)
{
    //This code sets the concatenated view and perspective projection matrices used for
    //our scene's camera view.
    //    gl_camera = Camera(w, h);
    gl_camera = Camera(w, h, glm::vec3(scene.dimensions.x/2, scene.dimensions.y/2, scene.dimensions.z/2),
                       glm::vec3(scene.dimensions.x/2, scene.dimensions.y/2+2, scene.dimensions.z/2+10), glm::vec3(0,1,0));
    glm::mat4 viewproj = gl_camera.getViewProj();
    //    initPlayerPos(&scene, &gl_camera);
    gl_camera.ToggleFlying();


    // Upload the view-projection matrix to our shaders (i.e. onto the graphics card)

    prog_lambert.setViewProjMatrix(viewproj);
    prog_flat.setViewProjMatrix(viewproj);

    printGLErrorLog();
}

//This function is called by Qt any time your GL window is supposed to update
//For example, when the function updateGL is called, paintGL is called implicitly.
//DO NOT CONSTRUCT YOUR SCENE GRAPH IN THIS FUNCTION!
void MyGL::paintGL()
{

    if (is_night) {
        if (is_cloudy) {
            glClearColor(0.25f, 0.25f, 0.25f, 1);
        } else {
            glClearColor(0.098f, 0.098f, 0.439f, 1);
        }
    }
    else {
        if (is_cloudy) {
            glClearColor(0.5f, 0.5f, 0.5f, 1);
        } else {
            glClearColor(0.37f, 0.74f, 1.0f, 1);
        }
    }
    // Clear the screen so that we only see newly drawn images
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    prog_flat.setViewProjMatrix(gl_camera.getViewProj());
    prog_lambert.setViewProjMatrix(gl_camera.getViewProj());

    glUseProgram(prog_lambert.prog);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureHandle);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, text_width, text_height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    prog_lambert.setTexture();

    //    glActiveTexture(GL_TEXTURE1);
    //    glBindTexture(GL_TEXTURE_2D, normalHandle);
    //    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, text_width, text_height, 0, GL_RGB, GL_UNSIGNED_BYTE, normal_map);
    //    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    //    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    //    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    //    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    //    prog_lambert.setNormalMap();
    GLDrawScene();
}

void MyGL::GLDrawScene()
{


    for(auto const& ent1 : scene.terrinChunkMap) {
        Chunk *chunk =  ent1.second;

        if(!chunk->updated){
            chunk->create();
            chunk->updated = true;
        }
        prog_lambert.setModelMatrix(glm::mat4());
        //prog_lambert.setTexture();
        if(chunk->isInRadius){
            prog_lambert.draw_chunk(*chunk);
        }
    }

    //Moving the sheep around the world
    int counter = 0;
    for(MovingObj *obj : movingObjs)
    {
        if(obj->isClose()){
            if(counter != 1){
                obj->setDestination( gl_camera.eye.x + 10, 0, gl_camera.eye.z + rand()%100 +10);
            }else{
                obj->setDestination(gl_camera.eye.x , gl_camera.eye.y ,gl_camera.eye.z);
            }

            obj->navigate(&scene);
        } else{
            obj->navigate(&scene);
        }

        prog_lambert.setModelMatrix(glm::translate(glm::mat4(), glm::vec3(obj->pos.x, obj->pos.y + 2 , obj->pos.z)));

        if(glm::distance(gl_camera.eye, obj->pos) < 100){
            prog_lambert.draw(geom_cube);
            prog_lambert.setModelMatrix(glm::translate(glm::mat4(), glm::vec3(obj->pos.x - 1, obj->pos.y + 2 , obj->pos.z)));
            prog_lambert.draw(geom_cube);
        }


        ++counter;
    }

    for(int i = 0; i < weatherObjs.size(); i++)
    {
        BlockType b = weatherObjs[i]->Type();
        if (b == SNOW) {
            weatherObjs[i]->fall(0.06, glm::vec3(0));
        } else if (b == WATER ) {
            weatherObjs[i]->fall(0.25, glm::vec3(0));
        } else if (b == LAVA ) {
            weatherObjs[i]->fall(0.15, glm::vec3(0));
        }
        prog_flat.setScreen(false);
        glm::vec3 pos = weatherObjs[i]->Pos();

        prog_flat.setModelMatrix(glm::translate(glm::mat4(), pos));
        prog_flat.draw(geom_precip);

        int x1 = (floor(pos.x/(float)CHUNK_SIZE)) * CHUNK_SIZE;
        int y1 = (floor(pos.y/(float)CHUNK_SIZE)) * CHUNK_SIZE;
        int z1 = (floor(pos.z/(float)CHUNK_SIZE)) * CHUNK_SIZE;
        BoundingBox box = std::make_tuple(x1,y1,z1,x1 + CHUNK_SIZE,y1+ CHUNK_SIZE,z1+ CHUNK_SIZE);

        std::map<BoundingBox, Chunk*>::iterator itm;
        itm = scene.terrinChunkMap.find(box);
        if(itm != scene.terrinChunkMap.end()) {
            Chunk *c = itm->second;
            std::map<std::tuple<int, int, int>, BlockType>::iterator itr;
            itr = c->cubeMap.find(std::make_tuple(pos.x,pos.y,pos.z));
            if (itr != c->cubeMap.end()) {
                BlockType type = c->cubeMap.at(std::make_tuple(pos.x,pos.y,pos.z));
                if (type != AIR) {
                    scene.addSpecialCube(pos.x, pos.y+1, pos.z, b);
                    c->isInRadius = true;
                    weatherObjs.erase(weatherObjs.begin() + i);
                }
            }
        }
    }

    //draw plus in center of screen
    prog_flat.setModelMatrix(glm::mat4());
    prog_flat.setScreen(true);
    glDisable(GL_DEPTH_TEST);
    prog_flat.draw(geom_plus);
    glEnable(GL_DEPTH_TEST);

}


void destroyBlock(Scene* scene, Camera* camera)
{
    glm::vec3 eye = camera->eye;
    glm::vec3 ray_direction = camera->look;

    float t = 0;
    glm::vec3 ray_pt = eye + t * ray_direction;

    while(t < 100)
    {
        //if block in that slot exists, destroy it
        ray_pt = eye + ((float)t) * ray_direction;

        int x = ray_pt[0] + 0.5;
        int y = ray_pt[1] + 0.5;
        int z = ray_pt[2] + 0.5;

        if (scene->cubeAt(x, y, z))
        {
            BoundingBox box = scene->boundingBoxAt(x, y, z);
            std::map<BoundingBox, Chunk*>::iterator itm =  scene->terrinChunkMap.find(box);
            if(itm != scene->terrinChunkMap.end()){
                Chunk* chunk = itm->second;
                chunk->cubeMap.erase(std::make_tuple(x,y,z));
                scene->addSpecialCube(x, y, z, AIR);
                chunk->isInRadius = true;
            }
            scene->createSceneBox(box);
            return;
        }

        t += 0.5;
    }
}

void addBlock(Scene* scene, Camera* camera)
{
    glm::vec3 eye = camera->eye;
    glm::vec3 ray_direction = camera->look;
    float t = 0;
    glm::vec3 ray_pt = eye + t * ray_direction;
    int x = ray_pt[0] + 0.5;
    int y = ray_pt[1] + 0.5;
    int z = ray_pt[2] + 0.5;
    int x_before = x;
    int y_before = y;
    int z_before = z;
    while(t < 100)
    {
        ray_pt = eye + ((float)t) * ray_direction;

        x = ray_pt[0];
        y = ray_pt[1];
        z = ray_pt[2];

        if(scene->cubeAt(x, y, z))
        {
            scene->addSpecialCube(x_before, y_before, z_before, BlockType::GRASS);
            BoundingBox box = scene->boundingBoxAt(x_before, y_before, z_before);
            scene->createSceneBox(box);
            return;
        }
        x_before = x;
        y_before = y;
        z_before = z;

        t += 0.5;
    }
}



void MyGL::mousePressEvent(QMouseEvent *e)
{
    if (e->button() == Qt::LeftButton)
    {
        destroyBlock(&scene, &gl_camera);
        update();
    }
    else if (e->button() == Qt::RightButton)
    {
        addBlock(&scene, &gl_camera);
        update();
    }
}

void  MyGL::detectCollision(glm::vec3 movement)
{
    int topPoint = scene.toppestPointAt(gl_camera.eye.x, gl_camera.eye.y, gl_camera.eye.z);


    //Check the ground
    if(!gl_camera.Flying()){
        if (topPoint - gl_camera.ground_level > 0 && topPoint - gl_camera.ground_level < 3) {
            gl_camera.TranslateWorldY(2);
        } else if (topPoint - gl_camera.ground_level > 3){
            gl_camera.TranslateAlongLookGrounded(-movement.z);
            gl_camera.TranslateAlongRight(-movement.x);
        }


        while (gl_camera.ground_level > topPoint) {

            gl_camera.TranslateWorldY(-gl_camera.velocity);

            if (gl_camera.velocity < TERMINAL_VELOCITY)
            {
                gl_camera.velocity += ACCELERATION;
            }
        }
        return;
    }
    //    //Detect moving
    if(scene.cubeAt(gl_camera.eye.x, gl_camera.eye.y, gl_camera.eye.z)){
        gl_camera.TranslateAlongLookGrounded(-movement.z);
        gl_camera.TranslateAlongRight(-movement.x);
    }
    gl_camera.RecomputeAttributes();
}

void MyGL::timerUpdate()
{
    updatingClock += 0.01;
    timeCount++;
    clock++;
    if (timeCount % 10 == 0) {
        if (timeCount >= 1000) { timeCount = 0; }
        prog_lambert.setTime(timeCount % 16);
    }
    if (clock % 500 == 0) {
        if (clock > 6000) {
            clock = 0;
            if (is_night) {
                is_night = false;
            }
            else {
                is_night = true;
            }

        }



        // calculate new sun position (light direction)
        if (!is_night) {
            float degree = (clock / 6000.0) * 180;
            vec4 sun_pos = glm::rotate(mat4(1.0f), radians(-1 * degree), vec3(1, 0, -1)) * vec4(1, 0, 1, 0);
            prog_lambert.setLightDir(sun_pos);
        }
        else {
            prog_lambert.setLightDir(vec4(1, 0, 1, 0));
        }
    }

    if (clock % 1500 == 0) {
        if (clock  % 3000 == 0 ) {
            int r = rand() % 3;
            if (r == 0) {
                populateWeather(SNOW);
                geom_precip.setColor(weatherObjs[0]->Color());
            } else if (r == 1) {
                populateWeather(WATER);
                geom_precip.setColor(weatherObjs[0]->Color());
            } else if (r == 2) {
                populateWeather(LAVA);
                geom_precip.setColor(weatherObjs[0]->Color());
            }
            is_cloudy = true;
        } else {
            is_cloudy = false;
        }
    }


    float w = this->width() / 2.0f;
    float h = this->height() / 2.0f;

    // screen coords
    float x = QWidget::mapFromGlobal(QCursor::pos()).x();
    float y = QWidget::mapFromGlobal(QCursor::pos()).y();
    // ndc
    x = x / w - 1;
    y = 1 - y / h;

    float amt = 6.0f;
    if (x > -1 && y > -1 && x < 1  && y < 1)
    {
        gl_camera.RotateAboutUp(amt * -x);
        gl_camera.RotateAboutRight(amt * y);

    }

    gl_camera.RecomputeAttributes();
    update();
}

void MyGL::keyPressEvent(QKeyEvent *e)
{
    glm::vec3 movement = glm::vec3(0,0,0);

    float amount = 0.7f;
    if(e->modifiers() & Qt::ShiftModifier){
        amount = 10.0f;
    }
    // http://doc.qt.io/qt-5/qt.html#Key-enum
    // This could all be much more efficient if a switch
    // statement were used, but I really dislike their
    // syntax so I chose to be lazy and use a long
    // chain of if statements instead
    if (e->key() == Qt::Key_Escape) {
        QApplication::quit();
    } else if (e->key() == Qt::Key_Right) {

        gl_camera.RotateAboutUp(-amount);

    } else if (e->key() == Qt::Key_Left) {

        gl_camera.RotateAboutUp(amount);

    } else if (e->key() == Qt::Key_Up) {

        gl_camera.RotateAboutRight(-amount);

    } else if (e->key() == Qt::Key_Down) {

        gl_camera.RotateAboutRight(amount);

    } else if (e->key() == Qt::Key_1) {

        gl_camera.fovy += amount;

    } else if (e->key() == Qt::Key_2) {

        gl_camera.fovy -= amount;

    } else if (e->key() == Qt::Key_W) {

        gl_camera.TranslateAlongLookGrounded(1);
        movement = glm::vec3(0,0,1);
        play_footstep();

    } else if (e->key() == Qt::Key_S) {

        gl_camera.TranslateAlongLookGrounded(-1);
        movement = glm::vec3(0,0,-1);
        play_footstep();

    } else if (e->key() == Qt::Key_D) {

        gl_camera.TranslateAlongRight(1);
        movement = glm::vec3(1,0,0);
        play_footstep();

    } else if (e->key() == Qt::Key_A) {

        gl_camera.TranslateAlongRight(-1);
        movement = glm::vec3(-1,0,0);
        play_footstep();

    } else if (e->key() == Qt::Key_Q) {

        gl_camera.TranslateWorldY(-amount);
        movement = glm::vec3(0,-amount,0);

    } else if (e->key() == Qt::Key_E) {

        gl_camera.TranslateWorldY(amount);
        movement = glm::vec3(0,amount,0);

    } else if (e->key() == Qt::Key_R) {

        gl_camera = Camera(this->width(), this->height());

    } else if (e->key() == Qt::Key_F) {
        gl_camera.ToggleFlying();
        if(!gl_camera.Flying()){
            detectCollision(movement);
        }
    } else if (e->key() == Qt::Key_P) {
        play_music();
    } else if (e->key() == Qt::Key_O) {
        stop_music();
    }
    gl_camera.RecomputeAttributes();
    if (movement != glm::vec3(0,0,0)) {
        detectCollision(movement);
        if( gl_camera.eye.y > 32){
            scene.CheckScene(gl_camera.eye.x,  0, gl_camera.eye.z);
            scene.CheckScene(gl_camera.eye.x,  CHUNK_SIZE, gl_camera.eye.z);
        } else {
            scene.CheckScene(gl_camera.eye.x,  gl_camera.eye.y, gl_camera.eye.z);
        }

    }
    check_sounds();
    play_wind();
    update();  // Calls paintGL, among other things
}

void MyGL::slot_receiveSeed(int value){

    scene.seed = value;
}
void MyGL::slot_receiveGenerate(){

    scene.regenerateScene();
    emit signal_sendSeed(scene.seed);
}

void MyGL::slot_receiveWorld(){
    scene.autoGeneratTerrionToggle = !scene.autoGeneratTerrionToggle;
}

void MyGL::play_footstep() {
    if (step_sound.isFinished() && !music_playing && !gl_camera.Flying()) {
        step_sound.play();
    }
}

void MyGL::play_water() {
    if (water_sound.isFinished() && !music_playing) {
        water_sound.play();
    }
}

void MyGL::stop_water() {
    water_sound.stop();
}

void MyGL::play_wind() {
    if (wind_sound.isFinished() && !music_playing) {
        wind_sound.play();
    }
}

void MyGL::play_music() {
    misty_mountains.setLoops(1000);
    misty_mountains.play();
    music_playing = true;
}

void MyGL::stop_music() {
    misty_mountains.stop();
    music_playing = false;
}

void MyGL::check_sounds() {
    float x = gl_camera.eye.x;
    float y = gl_camera.eye.y-4;
    float z = gl_camera.eye.z;
    int x1 = (floor(x/(float)CHUNK_SIZE)) * CHUNK_SIZE;
    int y1 = (floor(y/(float)CHUNK_SIZE)) * CHUNK_SIZE;
    int z1 = (floor(z/(float)CHUNK_SIZE)) * CHUNK_SIZE;
    BoundingBox box = std::make_tuple(x1,y1,z1,x1 + CHUNK_SIZE,y1+ CHUNK_SIZE,z1+ CHUNK_SIZE);

    std::map<BoundingBox, Chunk*>::iterator itm;
    itm = scene.terrinChunkMap.find(box);

    if(itm != scene.terrinChunkMap.end()){
        Chunk *c = itm->second;
        std::map<std::tuple<int, int, int>, BlockType>::iterator itr;
        itr = c->cubeMap.find(std::make_tuple(x,y,z));
        if (itr != c->cubeMap.end()) {
            BlockType type = c->cubeMap.at(std::make_tuple(x,y,z));
            if (type == WATER) {
                play_water();
            }
            else {
                stop_water();
            }
        }
    }
}

