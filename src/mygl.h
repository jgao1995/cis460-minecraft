#ifndef MYGL_H
#define MYGL_H

#include <glwidget277.h>
#include <utils.h>
#include <shaderprogram.h>
#include <scene/cube.h>
#include "camera.h"
#include <scene/scene.h>
#include <QSound>

#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>

#include "scene/plus.h"
#include "scene/chunk.h"
#include "scene/movingobj.h"
#include "scene/precipitation.h"
#include "scene/weather.h"

static const int NUM_OFSHEEPS = 10;
const float TERMINAL_VELOCITY = 1;
const float ACCELERATION = 0.1;
class MyGL
        : public GLWidget277
{
private:
    Cube geom_cube;// The instance of a unit cube we can use to render any cube. Should NOT be used in final version of your project.
    ShaderProgram prog_lambert;// A shader program that uses lambertian reflection
    ShaderProgram prog_flat;// A shader program that uses "flat" reflection (no shadowing at all)

    GLuint vao; // A handle for our vertex array object. This will store the VBOs created in our geometry classes.
    // Don't worry too much about this. Just know it is necessary in order to render geometry.


    Camera gl_camera;
    Scene scene;
    std::vector<MovingObj*> movingObjs;
    //Chunk c;

    Plus geom_plus; // instance of center of screen plus

    Precipitation geom_precip;
    std::vector<Weather*> weatherObjs;

    /// Timer linked to timerUpdate(). Fires approx. 60 times per second
    QTimer timer;


public:
    explicit MyGL(QWidget *parent = 0);
    ~MyGL();
    float updatingClock;

    int timeCount = 0;
    int clock = 0;
    unsigned char* image;
    unsigned char* normal_map;
    int text_width;
    int text_height;
    GLuint textureHandle;
    GLuint normalHandle;
    bool is_night = false;
    bool is_cloudy = false;

    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    void GLDrawScene();
//    QSound water_sound;
    QSound step_sound;
    QSound water_sound;
    QSound wind_sound;
    QSound misty_mountains;
    bool water_playing = false;
    void check_sounds();
    void play_footstep();
    void play_water();
    void stop_water();
    void play_wind();
    void play_music();
    void stop_music();
    bool music_playing = false;

    void detectCollision(glm::vec3);

    void populateWeather(BlockType b);

public slots:
    virtual void slot_receiveSeed(int);
    virtual void slot_receiveGenerate();
    virtual void slot_receiveWorld();

signals:

protected:
    void keyPressEvent(QKeyEvent *e);
    void mousePressEvent(QMouseEvent*);

private slots:
    /// Slot that gets called ~60 times per second
    virtual void timerUpdate();
};


#endif // MYGL_H
