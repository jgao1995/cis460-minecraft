#include "camera.h"

#include <la.h>
#include <iostream>


Camera::Camera():
    Camera(400, 400)
{
    look = glm::vec3(0,0,-1);
    up = glm::vec3(0,1,0);
    right = glm::vec3(1,0,0);
}

Camera::Camera(unsigned int w, unsigned int h):
    Camera(w, h, glm::vec3(0,0,10), glm::vec3(0,0,0), glm::vec3(0,1,0))
{}

Camera::Camera(unsigned int w, unsigned int h, const glm::vec3 &e, const glm::vec3 &r, const glm::vec3 &worldUp):
    fovy(45),
    width(w),
    height(h),
    near_clip(0.1f),
    far_clip(1000),
    eye(e),
    ref(r),
    world_up(worldUp),
    ground_level(eye[1]),
    velocity(0),
    flying(false)
{
    RecomputeAttributes();

}

Camera::Camera(const Camera &c):
    fovy(c.fovy),
    width(c.width),
    height(c.height),
    near_clip(c.near_clip),
    far_clip(c.far_clip),
    aspect(c.aspect),
    eye(c.eye),
    ref(c.ref),
    look(c.look),
    up(c.up),
    right(c.right),
    world_up(c.world_up),
    V(c.V),
    H(c.H)
{}

void Camera::ToggleFlying()
{
    if(flying)
    {
        flying = false;
    }
    else
    {
        flying = true;
    }
    velocity = 0;
    RecomputeAttributes();
}

bool Camera::Flying()
{
    return flying;
}


void Camera::RecomputeAttributes()
{
    if (flying)
    {
        ground_level = eye[1] - 5;
    }
    else
    {
        // keeps player on the ground
        eye[1] = ground_level + 5;
    }

    look = glm::normalize(ref - eye);

    float dot = glm::dot(look, world_up);
    if ( dot < - 0.99)
    {
        RotateAboutRight(0.01);
    } else if (dot > 0.99)
    {
        RotateAboutRight(-0.01);
    }

    right = glm::normalize(glm::cross(look, world_up));
    up = glm::cross(right, look);

    float tan_fovy = tan(glm::radians(fovy/2));
    float len = glm::length(ref - eye);
    aspect = width/height;
    V = up*len*tan_fovy;
    H = right*len*aspect*tan_fovy;
}

glm::mat4 Camera::getViewProj()
{
    return glm::perspective(glm::radians(fovy), width / (float)height, near_clip, far_clip) * glm::lookAt(eye, ref, up);
}

void Camera::RotateAboutUp(float deg)
{
    glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), glm::radians(deg), up);
    ref = ref - eye;
    ref = glm::vec3(rotation * glm::vec4(ref, 1));
    ref = ref + eye;
    RecomputeAttributes();
}
void Camera::RotateAboutRight(float deg)
{
    glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), glm::radians(deg), right);
    ref = ref - eye;
    ref = glm::vec3(rotation * glm::vec4(ref, 1));
    ref = ref + eye;
    RecomputeAttributes();
}

void Camera::TranslateWorldZ(float z)
{
    eye.z += z;
    ref.z += z;
}

void Camera::TranslateWorldX(float x)
{
    eye.x += x;
    ref.x += x;
}

void Camera::TranslateWorldY(float y)
{
    eye.y += y;
    ref.y += y;
    ground_level += y;
}


void Camera::TranslateAlongLook(float amt)
{
    glm::vec3 translation = look * amt;
    eye += translation;
    ref += translation;
}

void Camera::TranslateAlongLookGrounded(float amt){
    glm::vec3 grounded = glm::normalize(glm::vec3(look.x, 0, look.z));

    glm::vec3 translation = grounded * amt;
    eye += translation;
    ref += translation;
}

void Camera::TranslateAlongRight(float amt)
{
    glm::vec3 translation = right * amt;
    eye += translation;
    ref += translation;

}
void Camera::TranslateAlongUp(float amt)
{
    glm::vec3 translation = up * amt;
    eye += translation;
    ref += translation;
}
