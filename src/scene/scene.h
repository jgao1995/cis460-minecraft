#pragma once
#include <QList>
#include <camera.h>
#include<set>
#include "perlinnoise.h"
#include "chunk.h"
class MyGL;
typedef std::tuple<int, int, int, int ,int, int> BoundingBox;

const static int CHUNK_SIZE = 16;
const static int DISTANCE_CHECK = 10;
const static int EXTEND_DISTANCE = DISTANCE_CHECK * DISTANCE_CHECK;
const static int TAXICABRADIUS = 8;
const static int NUMOFTUNNEL = 5;
const static int MOUNTAINHEIGHT = 128;
const static int SNOWLINE = 80;

class Scene
{
public:
    bool autoGeneratTerrionToggle;
    MyGL *mygl;
    BoundingBox curboudingBox;
    std::map<BoundingBox, Chunk*> terrinChunkMap;
    Scene();
    PerlinNoise n ;
    //    std::map<std::tuple<int, int, int>, BlockType> objectsMap; // The map that contians all the objects;
    int seed;
    std::vector<std::vector<glm::vec4>> rivers;
    std::vector<std::vector<glm::vec4>> rivers_left;
    std::vector<std::vector<glm::vec4>> rivers_right;
    std::vector<std::vector<glm::vec4>> rivers_end;

    void setMyGl(MyGL *gl);
    void CreateTestScene();
    void CheckScene(float x, float y, float z);

    void extendSceneLeft(int x1, int x2 , int z1, int z2, int y);
    void extendSceneRight(int x1, int x2 , int z1, int z2, int y);
    void extendSceneBackward(int x1, int x2 , int z1, int z2, int y);
    void extendSceneForward(int x1, int x2 , int z1, int z2, int y);
    void extendSceneUp(int x1, int x2 , int z1, int z2, int y);
    void extendSceneDown(int x1, int x2 , int z1, int z2, int y);

    void createSceneBox(BoundingBox box);
    void regenerateScene();

    void generateTaxiCabRadius();
    void generatePerlinTunnelsRadisu(int x, int y, int z);
    void generatePerlinCavess(int x, int y, int z);
    void generatePerlinTunnels(int x, int y, int z);

    BoundingBox boundingBoxAt(int x, int y, int z);
    void addSpecialCube(int x, int y, int z, BlockType typ);
    //Generate a 64/72/64 mountain area
    void generateMountains(int x, int z);
    //Old method to find the cube
    //    bool findCube(int x, int y, int z);

    void CreateRiver(std::string);
    void LevelBlocksUp(glm::vec4, int);
    void LevelBlocksPosX(glm::vec4, int);
    void LevelBlocksNegX(glm::vec4, int);
    void LevelBlocksPosZ(glm::vec4, int);
    void LevelBlocksNegZ(glm::vec4, int);
    void LevelRiverEnd(glm::vec4);
    void MakeSceneRivers();
    void generateLake(int x, int z);
    void generateTree(int x, int y, int z);
    void generateLava(int x, int y, int z);

    int toppestPointAt(float, float, float);
    bool cubeAt(float, float, float);
    BlockType get_cube_type(float x, float y, float z);

    glm::ivec3 dimensions;
};
