#ifndef CHUNK_H
#define CHUNK_H

#include "drawable.h"
#include <la.h>
#include <QOpenGLContext>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <string>
#include<set>
#include <iostream>

typedef std::tuple<int, int, int, int ,int, int> BoundingBox;
enum BlockType{GRASS, WATER, STONE, AIR, LAVA, LEAF, WOOD, IRON, COAL, GOLD, DIAMOND, BEDROCK, SAND, SNOW};
enum Direction{TOP, BOT, SIDE};
using namespace std;
using namespace glm;

struct Vertex {
    glm::vec4 pos;
    glm::vec4 norm;
    glm::vec4 col;
    glm::vec2 uv;
};

class Chunk: public Drawable {
public:

    Chunk();
    Chunk(GLWidget277* context);

    //if updates is false, then the cube will be recreated, then set to true meaning it is updated
    bool updated;

    //If not in radius, a chunk will not be drawn
    bool isInRadius;

    //If a chunk is not finished, it means it still continas air blocks and need to populated with drawable cubes
    bool isFinished;
    std::map<std::tuple<int, int, int>, BlockType> cubeMap;

    virtual void create();


    void addCube(std::tuple<int, int, int> pos, BlockType typ);


    GLuint buf_attr; // VBO that holds all vertex data

    bool attr_bound; // set to true once VBO has been bound

    void populate_vertex_data(vector<Vertex> *v);

    void set_texture(Vertex &v1, Vertex &v2, Vertex &v3, Vertex &v4, Direction d, BlockType typ, bool above_sea_level);

    int face_cnt;

    void generate_attr(); // calls glGenBuffers(1, buf_attr) on GPU

    bool bind_attr();

    bool cubeAt(float,float,float);

    BlockType get_cube_type(int, int, int);

};

#endif // CHUNK_H
