#ifndef MOVINGOBJ_H
#define MOVINGOBJ_H
#include <la.h>
#include "scene/scene.h"


const static float MOVING_SPEED = 0.05;
class MovingObj
{
public:
    MovingObj();
    MovingObj(int x, int y, int z);

    glm::vec3 pos;
    glm::vec3 destination;
    glm::vec4 rotation;

    bool closeFlag;
    float speed;




    void move(float x, float y, float z);
    void setDestination(float x, float y, float z);
    bool isClose();
    void navigate(Scene *s);

};

#endif // MOVINGOBJ_H
