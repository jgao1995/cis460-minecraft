#include <scene/scene.h>
#include <mygl.h>
#include <iostream>
#include <deque>
#include <stack>
#include <time.h>
#include <la.h>

static int MAX_HEIGHT = 50; // for river valleys
static bool TESTING_MODE = false;//false;
Scene::Scene() : dimensions(64, 64, 64),
    curboudingBox(std::make_tuple(0,0,0,16,16, 16)), seed(1000),
    terrinChunkMap(*(new std::map<BoundingBox, Chunk*>)), n(PerlinNoise(seed))
{

    srand(time(NULL));
    autoGeneratTerrionToggle = false;

}

void Scene::setMyGl(MyGL *gl){
    this->mygl = gl;
}

void Scene::CreateTestScene()
{

    if (!TESTING_MODE) {
        //        generatePerlinTunnels();
        generateMountains(0, 0);

        CreateRiver("+FFS[+FFX]-FFS[-FFX]"); //thin river
        //        CreateRiver("S[FF-X+F]X+FF"); //delta river

        MakeSceneRivers();
        ;
    }
    generateLake(30,0);
    generateMountains(0, 0);
    generatePerlinTunnels(10, 16, 10);
    generatePerlinTunnels(0, 5, 0);
    generatePerlinTunnels(5, 16, 5);
    generatePerlinTunnels(-5, 5, -5);
    generatePerlinTunnels(0, 0, 5);
    for(int i = 0; i < NUMOFTUNNEL; ++i){
        generatePerlinTunnels(rand() % 100 - 50, 0, rand() % 100 - 50);
    }
    //    Must generate the terrain last in order to set everything to be in radius
    generateTaxiCabRadius();

}

//Create the cave cubes
void Scene::generatePerlinTunnels(int x, int y, int z)
{

    int counter = 0;
    double  r = 1;
    while (counter <= 16 * 16 || r > 0.01){
        float noise = n.noiseAt((float)x/CHUNK_SIZE, (float)y/CHUNK_SIZE,(float)z/CHUNK_SIZE)/0.7;
        float direction = (noise + 1) * 180;
        if(noise > 0.1){
            y++;
        } else if(noise < -0.1){
            y--;
        }
        if(direction < 150){
            generatePerlinTunnelsRadisu(++x,y,z);
        } else if(direction < 180){
            generatePerlinTunnelsRadisu(x,y,--z);
        } else if(direction < 210){
            generatePerlinTunnelsRadisu(--x,y,z);
        } else if(direction < 360){
            generatePerlinTunnelsRadisu(x,y,++z);
        }
        counter++;
        if(counter >= 16 * 16){
            r = ((double) rand() / (RAND_MAX));
        }
        if(y < -128){
            break;
        }

    }




}


//Generate the Perlin noise tunnesls with a radius
void Scene::generatePerlinTunnelsRadisu(int x, int y, int z){
    float noise = n.noiseAt((float)x/64, (float)y/64,(float)z/64);
    if(noise >= 0.5){
        generatePerlinCavess(x,y,z);
    }

    int cubeRadius = rand()% 10 + 5;
    int tempx = x;
    int tempy = y;
    int tempz = z;
    for(int i =0; i < cubeRadius; ++i){
        for(int j = 0; j < cubeRadius; ++j ){
            for(int k = 0; k < cubeRadius; ++k ){
                addSpecialCube(x+i,y+j,z+k, AIR);

            }
        }
    }
}


//Generate the Perlin noise caves at points on the tunnel
void Scene::generatePerlinCavess(int x, int y, int z){
    //Generating the perlin cave


    int caveRadius = rand()% 100;
    for(int i =0; i < caveRadius; ++i){
        for(int j = 0; j < caveRadius; ++j ){
            for(int k = 0; k < caveRadius; ++k ){
                float noise = n.noiseAt((float)(x+i)/64, (float)(y+j)/64,(float)(z+k)/64);
                if(noise >= 0.1){

                    if(y + j  > 0){
                        addSpecialCube(x+i,y+j,z+k, AIR);
                    } else if(j < 5 ){
                        addSpecialCube(x+i,y+j,z+k, LAVA);
                    } else {
                        //Generating the coal iron glod based on the rand result
                        if(noise >= 0.59){
                            addSpecialCube((x+i),(y+j),(z+k),DIAMOND);
                        }else  if(noise >= 0.55){
                            addSpecialCube((x+i),(y+j),(z+k),GOLD);
                        }else if(noise >= 0.53){
                            addSpecialCube((x+i),(y+j),(z+k),IRON);
                        } else if(noise >= 0.5){
                            addSpecialCube((x+i),(y+j),(z+k),COAL);
                        } else {
                            addSpecialCube(x+i,y+j,z+k, AIR);
                        }
                    }

                }

            }
        }
    }

}

//Create the initial terrain
void Scene::generateTaxiCabRadius(){
    int x1 = std::get<0>(curboudingBox)- CHUNK_SIZE * (TAXICABRADIUS/2);
    int z1 = std::get<2>(curboudingBox) + CHUNK_SIZE * (TAXICABRADIUS/2);
    int x2 = std::get<3>(curboudingBox) - CHUNK_SIZE * (TAXICABRADIUS/2);
    int z2 = std::get<5>(curboudingBox) + CHUNK_SIZE * (TAXICABRADIUS/2);

    for(int i = 0; i <= TAXICABRADIUS; ++i)
    {
        for(int j = 0;j <= TAXICABRADIUS; ++j)
        {
            for(int y = -CHUNK_SIZE * 3; y < CHUNK_SIZE * 8; y+=CHUNK_SIZE){
                BoundingBox boxUp = std::make_tuple(x1 + CHUNK_SIZE*i, y, z1 + CHUNK_SIZE*j, x2 + CHUNK_SIZE*i, y + CHUNK_SIZE, z2 + CHUNK_SIZE*j);
                BoundingBox boxDown = std::make_tuple(x1 + CHUNK_SIZE*i, y, z1 - CHUNK_SIZE*j, x2 + CHUNK_SIZE*i, y + CHUNK_SIZE, z2 - CHUNK_SIZE*j);
                createSceneBox(boxUp);
                createSceneBox(boxDown);
            }

        }
    }

}

//Create/repopulate the terrin block according to the bounding box
void Scene::createSceneBox(BoundingBox box){

    //Check if the box is already renderred
    int x1 = std::get<0>(box);
    int y1 = std::get<1>(box);
    int z1 = std::get<2>(box);
    int x2 = std::get<3>(box);
    int y2 = std::get<4>(box);
    int z2 = std::get<5>(box);
    std::map<BoundingBox, Chunk*>::iterator itm;
    std::map<std::tuple<int, int, int>, BlockType>::iterator cubeItm;
    itm = terrinChunkMap.find(box);
    Chunk *chunk;
    if(itm != terrinChunkMap.end()){
        chunk = itm->second;
        if(chunk->isFinished){
            chunk->isInRadius= true;
            return;
        }
    } else{
        chunk = new Chunk(mygl);
        terrinChunkMap.insert(std::pair<BoundingBox, Chunk*>(box, chunk));
    }

    if(y1 < -128){
        return;
    }

    if(y1 >= 0) {
        for(int x = x1; x < x2; x++)
        {
            for(int z = z1; z < z2; z++)
            {

                int height = (n.noiseAt((float)x/dimensions.x, 0,(float)z/dimensions.x) + 0.7)/ 0.9 * dimensions.y/2;
                if(height <= 0 ){
                    ++height;
                }
                for(int y = y1; y < y2; y++)
                {
                    cubeItm = chunk->cubeMap.find(std::make_tuple(x,y,z));
                    if(cubeItm == chunk->cubeMap.end()){
                        if(height > y){
                            if(y > height - 3)
                            {
                                chunk->addCube(std::make_tuple(x,y,z), GRASS);
                            } else {
                                chunk->addCube(std::make_tuple(x,y,z), STONE);

                            }

                        }

                    } else{
                        BlockType bt =  cubeItm->second;
                        if(bt == AIR){
                            chunk->cubeMap.erase(std::make_tuple(x,y,z));
                            chunk->updated = false;
                        }
                    }
                }

            }
        }
    } else if (y1 < 0){
        //     world below the sea line
        for(int x = x1; x < x2; x++)
        {
            for(int z = z1; z < z2; z++)
            {
                for(int y = y1; y < y2; y++)
                {

                    float perlin = n.noiseAt((float)x/dimensions.x, (float)y/dimensions.x,(float)z/dimensions.x);

                    cubeItm = chunk->cubeMap.find(std::make_tuple(x,y,z));
                    if(cubeItm == chunk->cubeMap.end()){
                        if(perlin < -0.55){
                            chunk->addCube(std::make_tuple(x,y,z), DIAMOND);
                        } else if (perlin < -0.5){
                            chunk->addCube(std::make_tuple(x,y,z), IRON);
                        }else if (perlin < -0.45){
                            chunk->addCube(std::make_tuple(x,y,z), COAL);

                        }else if (perlin > 0.5){
                            chunk->addCube(std::make_tuple(x,y,z), LAVA);

                        } else {
                            chunk->addCube(std::make_tuple(x,y,z), STONE);

                        }


                    } else{
                        BlockType bt =  cubeItm->second;
                        if(bt == AIR){
                            chunk->cubeMap.erase(std::make_tuple(x,y,z));
                            chunk->updated = false;
                        }
                    }




                }
            }

        }
    }

    //Insert bedrock if it is in the end
    if(y1 == -128){
        for(int x = x1; x < x2; x++)
        {
            for(int z = z1; z < z2; z++)
            {
                chunk->addCube(std::make_tuple(x,y1,z), BEDROCK);
            }
        }
    }
    chunk->isInRadius = true;
    chunk->isFinished = true;


}


//adds the rivers to the scene
void Scene::MakeSceneRivers() {
    int offset = 0;
    for (int i = 0; i < rivers.size(); i ++) {
        std::vector<glm::vec4> blocks = rivers[i];
        std::vector<glm::vec4> blocks_right = rivers_right[i];
        std::vector<glm::vec4> blocks_left = rivers_left[i];
        std::vector<glm::vec4> blocks_end = rivers_end[i];

        for (int i = 1; i < blocks.size(); i++)
        {
            int x = offset - blocks[i].x;
            int y = blocks[i].y;
            int z = offset - blocks[i].z;
            addSpecialCube(x,y,z, WATER);
            LevelBlocksUp(glm::vec4(offset-blocks[i].x - offset,blocks[i].y,offset-blocks[i].z - offset,1), 0);
        }

        int prev_x = 0;
        int prev_z = 0;
        for (int i = 0; i < blocks_right.size();i++) {
            int x = offset-blocks_right[i].x;
            int y = blocks_right[i].y;
            int z = offset-blocks_right[i].z;
            glm::vec4 pos = glm::vec4(x, y, z, 1);
            LevelBlocksPosX(pos, 0);
            LevelBlocksPosZ(pos, 0);
            LevelBlocksNegZ(pos, 0);

            prev_x = x;
            prev_z = z;
        }

        prev_x = 0;
        prev_z = 0;
        for (int i = 0; i < blocks_left.size();i++) {
            int x = offset-blocks_left[i].x;
            int y = blocks_left[i].y;
            int z = offset-blocks_left[i].z;
            glm::vec4 pos = glm::vec4(x, y, z, 1);
            LevelBlocksNegX(pos, 0);
            LevelBlocksPosZ(pos, 0);
            LevelBlocksNegZ(pos, 0);

            prev_x = x;
            prev_z = z;
        }

        for (int i = 0; i < blocks_end.size();i++) {
            int x = offset-blocks_end[i].x;
            int y = blocks_end[i].y;
            int z = offset-blocks_end[i].z;
            glm::vec4 pos = glm::vec4(x, y, z, 1);
            LevelRiverEnd(pos);
        }
        offset+=100;
    }


}

//creates river positions using L-Systems based on the given rule
void Scene::CreateRiver(std::string rule)
{
    std::vector<glm::vec4> blocks = std::vector<glm::vec4>();
    std::vector<glm::vec4> blocks_left = std::vector<glm::vec4>();
    std::vector<glm::vec4> blocks_right = std::vector<glm::vec4>();
    std::vector<glm::vec4> blocks_end = std::vector<glm::vec4>();

    std::deque<char> instr = std::deque<char>();
    instr.push_back('F');
    instr.push_back('X');

    std::stack<glm::vec4> pos_stack = std::stack<glm::vec4>();
    glm::vec4 pos = glm::vec4(0,0,0,1);
    glm::vec4 prev_pos = pos;
    int sign = 1;
    float angle;
    float length;
    float horiz;
    float PI = 3.1415926;
    int rec_max = 10;//20; // Num of times to replace X
    int rec_min = 5;//10;
    int rec_count = 0;
    int width = 5;//10;

    blocks_end.push_back(glm::vec4(pos.x + (float)width/2, pos.y, pos.z, 1));

    while(!instr.empty())
    {
        char c = instr.front();
        instr.pop_front();
        switch(c)
        {
        case 'F':
        {
            angle = (rand() % 5 + 1)/100.0f;//  * sign;
            length = (rand() % 15 + 1);//10.0f;
            horiz = (rand() % 5 + 1)*sign;
            glm::mat4 tmat = glm::mat4();
            tmat = glm::translate(tmat, glm::vec3(prev_pos.x-pos.x,prev_pos.y-pos.y,prev_pos.z-pos.z));
            tmat = glm::rotate(tmat, angle, glm::vec3(0,1 * sign,0));
            tmat = glm::translate(tmat, -glm::vec3(prev_pos.x-pos.x,prev_pos.y-pos.y,prev_pos.z-pos.z));
            tmat = glm::translate(tmat, glm::vec3(horiz, 0, length));

            prev_pos = pos;
            pos = tmat * pos;
            float dist  =(glm::distance(pos, prev_pos));
            glm::vec4 slope = (pos - prev_pos) / dist;
            for (int i = 0; i < dist; i++) {
                glm::vec4 new_pos = prev_pos + ((float)i) * slope;
                blocks_left.push_back(new_pos);
                blocks_right.push_back(glm::vec4(new_pos.x+width, new_pos.y,new_pos.z,1));
                for (int j = 0; j < width; j++) {
                    blocks.push_back(glm::vec4(new_pos.x+j, new_pos.y,new_pos.z,1));
                }
            }

            blocks_left.push_back(pos);
            blocks_right.push_back(glm::vec4(pos.x+width, pos.y,pos.z,1));
            for (int j = 0; j < width; j++) {
                blocks.push_back(glm::vec4(pos.x+j, pos.y,pos.z,1));
            }
            break;
        }
        case 'S':
        {
            pos_stack.push(prev_pos);
            pos_stack.push(pos);
            if (width>2) {
                width--;
            }
            break;
        }
        case '-':
        {
            sign = -1;
            break;
        }
        case '+':
        {
            sign = 1;
            break;
        }
        case 'X':
        {
            int r = rand();
            if (rec_count < rec_min || (r % 10 > 5 && rec_count < rec_max))
            {
                for (int i = rule.length() - 1; i >= 0; i--)
                {
                    instr.push_front(rule[i]);
                }
                rec_count++;
            }
            break;
        }
        case ']':
        {
            blocks_end.push_back(glm::vec4(pos.x + (float)width/2, pos.y, pos.z, 1));
            pos = pos_stack.top();
            pos_stack.pop();
            prev_pos = pos_stack.top();
            pos_stack.pop();
            width++;
            break;
        }
        }
    }

    rivers.push_back(blocks);
    rivers_right.push_back(blocks_right);
    rivers_left.push_back(blocks_left);
    rivers_end.push_back(blocks_end);
}


//level out the end of a river branch
void Scene::LevelRiverEnd(glm::vec4 block) {
    int x = block.x;
    int y = block.y;
    int z = block.z;

    int x_bound = 3;
    int z_bound = 1;
    int h = y;
    float ct = 1;
    while (h < MAX_HEIGHT) {
        for (int i = -x_bound - 1; i < x_bound + 1; i < i++) {
            LevelBlocksUp(glm::vec4(x+i, h, z + z_bound, 1), 0);
            LevelBlocksUp(glm::vec4(x+i, h, z - z_bound, 1), 0);
        }
        for (int i = -z_bound - 1; i < z_bound + 1;i++) {
            LevelBlocksUp(glm::vec4(x+x_bound, h, z + i, 1), 0);
            LevelBlocksUp(glm::vec4(x-x_bound, h, z +i, 1), 0);
        }
        if ((z_bound % (int) ct) == 0) {
            h++;
        }
        if (ct < 5) {
            ct+=0.1;
        }
        x_bound++;
        z_bound++;
    }
}



//levels out a block's neighbors based on a certain height
void Scene::LevelBlocksUp(glm::vec4 block, int height)
{
    int x = block.x;
    int y = block.y;
    int z = block.z;

    int h = y + height;
    while (h < MAX_HEIGHT) {
        addSpecialCube(x, h, z, AIR);
        h++;
    }
}

//levels out a block's neighbors based on a certain height
void Scene::LevelBlocksPosX(glm::vec4 block, int height)
{
    int x = block.x;
    int y = block.y;
    int z = block.z;

    float h = y + height;
    float dh = 0;
    while (h < MAX_HEIGHT) {
        for (int i = block.x; i < x; i ++) {
            addSpecialCube(i, h, z, AIR);

        }
        x+=dh;
        h++;
        dh+=0.1;

    }

}

//levels out a block's neighbors based on a certain height
void Scene::LevelBlocksNegX(glm::vec4 block, int height)
{
    int x = block.x;
    int y = block.y;
    int z = block.z;

    float h = y + height;
    float dh = 0;
    while (h < MAX_HEIGHT) {
        for (int i = x; i < block.x; i ++) {
            addSpecialCube(i, h, z, AIR);

        }
        x-=dh;
        h++;
        dh+=0.1;

    }

}

//levels out a block's neighbors based on a certain height
void Scene::LevelBlocksPosZ(glm::vec4 block, int height)
{
    int x = block.x;
    int y = block.y;
    int z = block.z;

    float h = y + height;
    float dh = 0;
    while (h < MAX_HEIGHT) {
        for (int j = block.z; j < z; j ++) {
            addSpecialCube(x, h, j, AIR);
        }


        z+=dh;
        h++;
        dh+=0.1;
    }
}

//levels out a block's neighbors based on a certain height
void Scene::LevelBlocksNegZ(glm::vec4 block, int height)
{
    int x = block.x;
    int y = block.y;
    int z = block.z;

    float h = y + height;
    float dh = 0;
    while (h < MAX_HEIGHT) {
        for (int j = z;j < block.z; j ++) {
            addSpecialCube(x, h, j, AIR);
        }

        z-=dh;
        h++;
        dh+=0.1;
    }

}

void Scene::CheckScene(float x, float y, float z){


    if(autoGeneratTerrionToggle){
        return ;
    }
    std::map<BoundingBox, Chunk*>::iterator itm;
    int x1 = std::get<0>(curboudingBox);
    int y1 = std::get<1>(curboudingBox);
    int z1 = std::get<2>(curboudingBox);
    int x2 = std::get<3>(curboudingBox);
    int y2 = std::get<4>(curboudingBox);
    int z2 = std::get<5>(curboudingBox);

    int iup = y2 - y;
    int idown = y - y1;
    int ileft = x2 - x;
    int iright = x - x1;
    int iforward = z2 - z;
    int ibackward = z - z1;

    if(ileft < 0){
        curboudingBox = std::make_tuple(x1+CHUNK_SIZE,y1,z1,x2+CHUNK_SIZE,y2,z2);
        extendSceneLeft(x1, x2, z1, z2, y1);
        extendSceneLeft(x1, x2, z1, z2, y1+CHUNK_SIZE);
        extendSceneLeft(x1, x2, z1, z2, y1+CHUNK_SIZE*2);
        extendSceneLeft(x1, x2, z1, z2, y1+CHUNK_SIZE*3);
        extendSceneLeft(x1, x2, z1, z2, y1+CHUNK_SIZE*4);
        extendSceneLeft(x1, x2, z1, z2, y1-CHUNK_SIZE);
        extendSceneLeft(x1, x2, z1, z2, y1-CHUNK_SIZE*2);
        extendSceneLeft(x1, x2, z1, z2, y1-CHUNK_SIZE*3);

    }
    if(iright < 0){
        curboudingBox =std::make_tuple(x1-CHUNK_SIZE,y1,z1,x2-CHUNK_SIZE,y2,z2);
        extendSceneRight(x1, x2, z1, z2, y1);
        extendSceneRight(x1, x2, z1, z2, y1+CHUNK_SIZE);
        extendSceneRight(x1, x2, z1, z2, y1+CHUNK_SIZE*2);
        extendSceneRight(x1, x2, z1, z2, y1+CHUNK_SIZE*3);
        extendSceneRight(x1, x2, z1, z2, y1+CHUNK_SIZE*4);
        extendSceneRight(x1, x2, z1, z2, y1-CHUNK_SIZE);
        extendSceneRight(x1, x2, z1, z2, y1-CHUNK_SIZE*2);
        extendSceneRight(x1, x2, z1, z2, y1-CHUNK_SIZE*3);



    }
    if(iforward < 0){
        curboudingBox = std::make_tuple(x1,y1,z1 +CHUNK_SIZE,x2,y2,z2 + CHUNK_SIZE);
        extendSceneForward(x1, x2, z1, z2, y1);
        extendSceneForward(x1, x2, z1, z2, y1+CHUNK_SIZE);
        extendSceneForward(x1, x2, z1, z2, y1+CHUNK_SIZE*2);
        extendSceneForward(x1, x2, z1, z2, y1+CHUNK_SIZE*3);
        extendSceneForward(x1, x2, z1, z2, y1+CHUNK_SIZE*4);
        extendSceneForward(x1, x2, z1, z2, y1-CHUNK_SIZE);
        extendSceneForward(x1, x2, z1, z2, y1-CHUNK_SIZE*2);
        extendSceneForward(x1, x2, z1, z2, y1-CHUNK_SIZE*3);

    }
    if(ibackward < 0){
        curboudingBox = std::make_tuple(x1,y1,z1 - CHUNK_SIZE,x2,y2,z2 - CHUNK_SIZE);
        extendSceneBackward(x1, x2, z1, z2, y1);
        extendSceneBackward(x1, x2, z1, z2, y1+CHUNK_SIZE);
        extendSceneBackward(x1, x2, z1, z2, y1+CHUNK_SIZE*2);
        extendSceneBackward(x1, x2, z1, z2, y1+CHUNK_SIZE*3);
        extendSceneBackward(x1, x2, z1, z2, y1+CHUNK_SIZE*4);
        extendSceneBackward(x1, x2, z1, z2, y1-CHUNK_SIZE);
        extendSceneBackward(x1, x2, z1, z2, y1-CHUNK_SIZE*2);
        extendSceneBackward(x1, x2, z1, z2, y1-CHUNK_SIZE*3);

    }
    if(iup < 0){
        curboudingBox = std::make_tuple(x1,y1+CHUNK_SIZE,z1 ,x2,y2+ CHUNK_SIZE,z2 );
        extendSceneUp(x1, x2, z1, z2, y1 + CHUNK_SIZE);
        extendSceneUp(x1, x2, z1, z2, y1 + CHUNK_SIZE*2);
    }

    if(idown < 0){

        curboudingBox = std::make_tuple(x1,y1-CHUNK_SIZE,z1 ,x2,y2- CHUNK_SIZE,z2 );
        generateTaxiCabRadius();
        extendSceneDown(x1, x2, z1, z2, y1 - CHUNK_SIZE);
        extendSceneDown(x1, x2, z1, z2, y1 - CHUNK_SIZE *2);
    }


}

void Scene::extendSceneLeft(int x1, int x2 , int z1, int z2, int y)
{
    x1 -= CHUNK_SIZE * (TAXICABRADIUS/2);
    z1 += CHUNK_SIZE * (TAXICABRADIUS/2);
    x2 -= CHUNK_SIZE * (TAXICABRADIUS/2);
    z2 += CHUNK_SIZE * (TAXICABRADIUS/2);

    std::map<BoundingBox, Chunk*>::iterator itm;
    for(int i = 0 ; i <= TAXICABRADIUS; ++i){
        BoundingBox boxUp = std::make_tuple(x1 , y, z1 - CHUNK_SIZE*i, x2, y+CHUNK_SIZE, z2 - CHUNK_SIZE*i);
        itm = terrinChunkMap.find(boxUp);
        if(itm != terrinChunkMap.end()){
            Chunk *c = itm->second;
            c->isInRadius = false;
        }

    }

    for(int i = 0; i <= TAXICABRADIUS; ++i){
        BoundingBox boxUp = std::make_tuple(x1 + CHUNK_SIZE * (1+ TAXICABRADIUS), y, z1 - CHUNK_SIZE*i , x2 + CHUNK_SIZE * (1+ TAXICABRADIUS), y+CHUNK_SIZE, z2 - CHUNK_SIZE*i);
        createSceneBox(boxUp);
    }



}

void Scene::extendSceneRight(int x1, int x2 , int z1, int z2, int y)
{
    x1 -= CHUNK_SIZE * (TAXICABRADIUS/2);
    z1 += CHUNK_SIZE * (TAXICABRADIUS/2);
    x2 -= CHUNK_SIZE * (TAXICABRADIUS/2);
    z2 += CHUNK_SIZE * (TAXICABRADIUS/2);

    std::map<BoundingBox, Chunk*>::iterator itm;
    for(int i = 0 ; i <= TAXICABRADIUS; ++i){
        BoundingBox boxUp = std::make_tuple(x1 + CHUNK_SIZE * TAXICABRADIUS, y, z1 - CHUNK_SIZE*i, x2+ CHUNK_SIZE * TAXICABRADIUS, y+CHUNK_SIZE, z2 - CHUNK_SIZE*i);
        itm = terrinChunkMap.find(boxUp);
        if(itm != terrinChunkMap.end()){
            Chunk *c = itm->second;
            c->isInRadius = false;
        }

    }

    for(int i = 0; i <= TAXICABRADIUS; ++i){
        BoundingBox boxUp = std::make_tuple(x1 - CHUNK_SIZE, y, z1 - CHUNK_SIZE*i , x2  - CHUNK_SIZE, y + CHUNK_SIZE, z2 - CHUNK_SIZE*i);
        createSceneBox(boxUp);
    }


}

void Scene::extendSceneForward(int x1, int x2 , int z1, int z2, int y)
{
    x1 -= CHUNK_SIZE * (TAXICABRADIUS/2);
    z1 += CHUNK_SIZE * (TAXICABRADIUS/2);
    x2 -= CHUNK_SIZE * (TAXICABRADIUS/2);
    z2 += CHUNK_SIZE * (TAXICABRADIUS/2);

    std::map<BoundingBox, Chunk*>::iterator itm;
    for(int i = 0 ; i <= TAXICABRADIUS; ++i){
        BoundingBox boxUp = std::make_tuple(x1+ CHUNK_SIZE*i, y, z1 - CHUNK_SIZE * TAXICABRADIUS, x2  + CHUNK_SIZE*i, y + CHUNK_SIZE, z2 - CHUNK_SIZE * TAXICABRADIUS);
        itm = terrinChunkMap.find(boxUp);
        if(itm != terrinChunkMap.end()){
            Chunk *c = itm->second;
            c->isInRadius = false;
        }
    }

    for(int i = 0; i <= TAXICABRADIUS; ++i){

        BoundingBox boxUp = std::make_tuple(x1 + CHUNK_SIZE*i, y, z1 + CHUNK_SIZE , x2 + CHUNK_SIZE*i, y+CHUNK_SIZE, z2 + CHUNK_SIZE);
        createSceneBox(boxUp);
    }
}

void Scene::extendSceneBackward(int x1, int x2 , int z1, int z2, int y)
{
    x1 -= CHUNK_SIZE * (TAXICABRADIUS/2);
    z1 += CHUNK_SIZE * (TAXICABRADIUS/2);
    x2 -= CHUNK_SIZE * (TAXICABRADIUS/2);
    z2 += CHUNK_SIZE * (TAXICABRADIUS/2);

    std::map<BoundingBox, Chunk*>::iterator itm;
    for(int i = 0 ; i <= TAXICABRADIUS; ++i){
        BoundingBox boxUp = std::make_tuple(x1 + CHUNK_SIZE*i, y, z1 , x2 + CHUNK_SIZE*i, y+CHUNK_SIZE, z2);
        itm = terrinChunkMap.find(boxUp);
        if(itm != terrinChunkMap.end()){
            Chunk *c = itm->second;
            c->isInRadius = false;
        }
    }

    for(int i = 0; i <= TAXICABRADIUS; ++i){
        BoundingBox boxUp = std::make_tuple(x1+ CHUNK_SIZE*i, y, z1 - CHUNK_SIZE * (TAXICABRADIUS+1), x2  + CHUNK_SIZE*i, y + CHUNK_SIZE, z2 -  CHUNK_SIZE * (TAXICABRADIUS+1));
        createSceneBox(boxUp);
    }
}


void Scene::extendSceneUp(int x1, int x2 , int z1, int z2, int y)
{
    x1 -= CHUNK_SIZE * (TAXICABRADIUS/2);
    z1 += CHUNK_SIZE * (TAXICABRADIUS/2);
    x2 -= CHUNK_SIZE * (TAXICABRADIUS/2);
    z2 += CHUNK_SIZE * (TAXICABRADIUS/2);

    for(int i = 0; i <= TAXICABRADIUS; ++i)
    {
        for(int j = 0;j <= TAXICABRADIUS; ++j)
        {
            BoundingBox boxUp = std::make_tuple(x1 + CHUNK_SIZE*i, y, z1 + CHUNK_SIZE*j, x2 + CHUNK_SIZE*i, y + CHUNK_SIZE, z2 + CHUNK_SIZE*j);
            BoundingBox boxDown = std::make_tuple(x1 + CHUNK_SIZE*i, y, z1 - CHUNK_SIZE*j, x2 + CHUNK_SIZE*i, y + CHUNK_SIZE, z2 - CHUNK_SIZE*j);
            createSceneBox(boxUp);
            createSceneBox(boxDown);

        }
    }


}

void Scene::extendSceneDown(int x1, int x2 , int z1, int z2, int y)
{
    x1 -= CHUNK_SIZE * (TAXICABRADIUS/2);
    z1 += CHUNK_SIZE * (TAXICABRADIUS/2);
    x2 -= CHUNK_SIZE * (TAXICABRADIUS/2);
    z2 += CHUNK_SIZE * (TAXICABRADIUS/2);

    for(int i = 0; i <= TAXICABRADIUS; ++i)
    {
        for(int j = 0;j <= TAXICABRADIUS; ++j)
        {
            BoundingBox boxUp = std::make_tuple(x1 + CHUNK_SIZE*i, y, z1 + CHUNK_SIZE*j, x2 + CHUNK_SIZE*i, y + CHUNK_SIZE, z2 + CHUNK_SIZE*j);
            BoundingBox boxDown = std::make_tuple(x1 + CHUNK_SIZE*i, y, z1 - CHUNK_SIZE*j, x2 + CHUNK_SIZE*i, y + CHUNK_SIZE, z2 - CHUNK_SIZE*j);
            createSceneBox(boxUp);
            createSceneBox(boxDown);

        }
    }

}



void Scene::regenerateScene(){
    n = PerlinNoise(seed);
    terrinChunkMap =  std::map<BoundingBox, Chunk*>();
    CreateTestScene();
}

void Scene::generateMountains(int x, int z){
    for(int i = 0; i < MOUNTAINHEIGHT; ++i){
        for(int j = 0; j < MOUNTAINHEIGHT; j++){
            int height = (n.noiseAt((float)(x+i)/dimensions.x, 0,(float)(z + j)/dimensions.x) + 0.5) * MOUNTAINHEIGHT;
            if(height <= 0 ){
                ++height;
            }
            for(int k = 0; k < MOUNTAINHEIGHT; ++k)
            {
                double r =   ((double) rand() / (RAND_MAX));
                if(n.noiseAt((float)(x+i)/dimensions.x, (float)k/dimensions.x,(float)(z + j)/dimensions.x) > -0.2){
                    if(height >= k){
                        if(k > SNOWLINE ){
                            if( r > 0.7){
                                addSpecialCube(x + i,k, z + j, SNOW);
                            } else if (r < 0.01) {
                                addSpecialCube(x + i,k, z + j, LAVA);
                            } else {
                                addSpecialCube(x + i,k, z + j, STONE);
                            }

                        } else {
                            addSpecialCube(x + i,k, z + j, GRASS);
                            if (r > 0.99 && k == height){
                                generateTree(x+i, k, z +j);
                            }

                        }
                    }
                }

            }
        }
    }
}

void Scene::generateLake(int x, int z){
    int length  = rand() % 200;
    //    int depth;
    int surface = (n.noiseAt((float)x/dimensions.x, 0,(float)z/dimensions.x) + 0.7)/ 0.9 * dimensions.y/4;

    for(int i = 0; i < length; ++i){
        for(int j = 0; j < length; ++j){
            float noise = n.noiseAt((float)(i+i)/dimensions.x, 0 , (float)(z +j)/dimensions.x);
            if(noise > 0.3){
                for(int k = surface; k >= -surface ; k--){
                    addSpecialCube(x + i, k, z + j, WATER);
                }
            } else if (noise > 0) {
                for(int k = surface; k >= 0 ; k--){
                    addSpecialCube(x + i, k, z + j, SAND);
                }

            }

        }
    }

}

void  Scene::generateTree(int x, int y, int z){
    int height = rand() % 15;

    //Tree trunk
    for(int i = 0; i < height; i++){
        addSpecialCube(x, y + i, z, WOOD);
    }

    //Tree top
    x -= height / 2;
    z += height / 2;
    for(int i = 0; i < height ; i++){
        for(int j = 0; j < height ; j++){
            for(int k = 0; k < height / 2; k++){
                addSpecialCube(x + i, y + height+ k, z - j, LEAF);
            }
        }
    }



}

BoundingBox Scene::boundingBoxAt(int x, int y, int z) {
    int x1 = (floor((float)x/(float)CHUNK_SIZE)) * CHUNK_SIZE;
    int y1 = (floor((float)y/(float)CHUNK_SIZE)) * CHUNK_SIZE;
    int z1 = (floor((float)z/(float)CHUNK_SIZE)) * CHUNK_SIZE;

    return std::make_tuple(x1,y1,z1,x1 + CHUNK_SIZE,y1+ CHUNK_SIZE,z1+ CHUNK_SIZE);

}

//Add cube with cube type, for pre configerations
void Scene::addSpecialCube(int x, int y, int z, BlockType typ)
{
    int x1 = (floor((float)x/(float)CHUNK_SIZE)) * CHUNK_SIZE;
    int y1 = (floor((float)y/(float)CHUNK_SIZE)) * CHUNK_SIZE;
    int z1 = (floor((float)z/(float)CHUNK_SIZE)) * CHUNK_SIZE;

    BoundingBox box = std::make_tuple(x1,y1,z1,x1 + CHUNK_SIZE,y1+ CHUNK_SIZE,z1+ CHUNK_SIZE);

    std::map<BoundingBox, Chunk*>::iterator itm;
    itm = terrinChunkMap.find(box);

    if(itm != terrinChunkMap.end()){
        Chunk *c = itm->second;
        if(typ == AIR){
            c->isFinished = false;
        }
        c->isInRadius = false;
        c->updated = false;
        c->addCube(std::make_tuple(x,y,z), typ);
    } else{
        Chunk *c = new Chunk(mygl);

        if(typ == AIR){
            c->isFinished = false;
        }
        c->isInRadius = false;

        c->updated = false;

        terrinChunkMap.insert(std::pair<BoundingBox, Chunk*>(box, c));
        c->addCube(std::make_tuple(x,y,z), typ);
    }
}


bool Scene::cubeAt(float x , float y, float z)
{
    int x1 = (floor(x/(float)CHUNK_SIZE)) * CHUNK_SIZE;
    int y1 = (floor(y/(float)CHUNK_SIZE)) * CHUNK_SIZE;
    int z1 = (floor(z/(float)CHUNK_SIZE)) * CHUNK_SIZE;
    BoundingBox box = std::make_tuple(x1,y1,z1,x1 + CHUNK_SIZE,y1+ CHUNK_SIZE,z1+ CHUNK_SIZE);

    std::map<BoundingBox, Chunk*>::iterator itm;
    itm = terrinChunkMap.find(box);

    if(itm != terrinChunkMap.end()){
        Chunk *c = itm->second;
        return c->cubeAt(x,y,z);
    } else{
        return false;
    }
}

BlockType Scene::get_cube_type(float x, float y, float z) {
    int x1 = (floor(x/(float)CHUNK_SIZE)) * CHUNK_SIZE;
    int y1 = (floor(y/(float)CHUNK_SIZE)) * CHUNK_SIZE;
    int z1 = (floor(z/(float)CHUNK_SIZE)) * CHUNK_SIZE;
    BoundingBox box = std::make_tuple(x1,y1,z1,x1 + CHUNK_SIZE,y1+ CHUNK_SIZE,z1+ CHUNK_SIZE);

    std::map<BoundingBox, Chunk*>::iterator itm;
    itm = terrinChunkMap.find(box);

    Chunk *c = itm->second;
    return c->get_cube_type((int)x, (int) y, (int) z);

}


int Scene::toppestPointAt(float x, float y, float z){
    if(y < -128){
        return 0;
    }
    int x1 = (floor(x/(float)CHUNK_SIZE)) * CHUNK_SIZE;
    int y1 = (floor(y/(float)CHUNK_SIZE)) * CHUNK_SIZE;
    int z1 = (floor(z/(float)CHUNK_SIZE)) * CHUNK_SIZE;
    BoundingBox box = std::make_tuple(x1,y1,z1,x1 + CHUNK_SIZE,y1+ CHUNK_SIZE,z1+ CHUNK_SIZE);

    std::map<BoundingBox, Chunk*>::iterator itm;
    itm = terrinChunkMap.find(box);

    if(itm != terrinChunkMap.end()){
        Chunk *c = itm->second;

        for (int i = y1+ CHUNK_SIZE - 1; i >= y1; --i )
        {
            if(c->cubeAt(x, i, z))
            {
                if(i == y1+ CHUNK_SIZE - 1){
                    if(cubeAt(x, y1 + CHUNK_SIZE +1,z)){
                        return toppestPointAt(x, y1 + CHUNK_SIZE,z);
                    }
                }
                return i;
            }
        }
        return toppestPointAt(x, y1 -CHUNK_SIZE,z);

    } else{
        return toppestPointAt(x, y1 -1,z);
    }


}
