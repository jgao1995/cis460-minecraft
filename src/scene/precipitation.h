#include "drawable.h"
#include <la.h>

#include <QOpenGLContext>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>

static const int CUB_IDX_COUNT = 36;
static const int CUB_VERT_COUNT = 24;
class Precipitation : public Drawable
{
private:
    glm::vec4 color;
    glm::vec4 cub_vert_col[CUB_VERT_COUNT];

public:
    Precipitation(GLWidget277* context) : Drawable(context), color(glm::vec4(1)){}
    virtual void create();
    void setColor(glm::vec4);
};
