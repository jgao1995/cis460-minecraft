#include "weather.h"
#include <stdlib.h>
#include <time.h>

Weather::Weather()
    : type(SNOW), pos(glm::vec3(0, 0, 0))
{
    fall_speed = rand() % 5;
}

Weather::Weather(float x, float y, float z, BlockType t)
    : type(t), pos(glm::vec3(x, y, z))
{
    fall_speed = rand() % 5;
}

void Weather::fall(float speed, glm::vec3 wind_dir){
    pos.y -= speed + 0.01 * fall_speed;
}

glm::vec3 Weather::Pos() {
    return pos;
}

BlockType Weather::Type() {
    return type;
}

glm::vec4 Weather::Color() {
    if (type == WATER) {
       return glm::vec4(0.3,.3,1,1);
    } else if (type == SNOW) {
       return  glm::vec4(1,1,1,1);
    } else if (type == LAVA) {
        return glm::vec4(0.5,0.1,0.1,1);
    }
}
