#include "chunk.h"
#include "cube.h"

Chunk::Chunk(GLWidget277* context):Drawable(context),cubeMap(*(new std::map<std::tuple<int, int, int>,  BlockType>)) {
    face_cnt = 0;
    updated = false;
    isInRadius = false;
    isFinished = false;
}

void Chunk::set_texture(Vertex &v1, Vertex &v2, Vertex &v3, Vertex &v4, Direction d, BlockType typ, bool above_sea_level) {
    float adj = 4.0/256;
    switch(typ) {
    case GRASS:
        switch(d) {
        case TOP:
            v1.uv = glm::vec2(8.0/16+adj, 2.0/16+adj);
            v2.uv = glm::vec2(9.0/16-adj, 2.0/16+adj);
            v3.uv = glm::vec2(9.0/16-adj, 3.0/16-adj);
            v4.uv = glm::vec2(8.0/16+adj, 3.0/16-adj);
            break;
        case SIDE:
            v1.uv = glm::vec2(3.0/16+adj, 1.0/16-adj);
            v2.uv = glm::vec2(4.0/16-adj, 1.0/16-adj);
            v3.uv = glm::vec2(3.0/16+adj, 0/16+adj);
            v4.uv = glm::vec2(4.0/16-adj, 0/16+adj);
            break;
        case BOT:
            v1.uv = glm::vec2(2.0/16, 0/16);
            v2.uv = glm::vec2(3.0/16, 0/16);
            v3.uv = glm::vec2(2.0/16, 1.0/16);
            v4.uv = glm::vec2(3.0/16, 1.0/16);
            break;
        }
        break;
    case STONE:
        v1.uv = glm::vec2(1.0/16+adj, 0/16+adj);
        v2.uv = glm::vec2(2.0/16-adj, 0/16+adj);
        v3.uv = glm::vec2(2.0/16-adj, 1.0/16-adj);
        v4.uv = glm::vec2(1.0/16+adj, 1.0/16-adj);
        break;
    case BEDROCK:
        v1.uv = glm::vec2(1.0/16+adj, 1.0/16+adj);
        v2.uv = glm::vec2(2.0/16-adj, 1.0/16+adj);
        v3.uv = glm::vec2(2.0/16-adj, 2.0/16-adj);
        v4.uv = glm::vec2(1.0/16+adj, 2.0/16-adj);
        break;
    case IRON:
        v1.uv = glm::vec2(1.0/16, 2.0/16);
        v2.uv = glm::vec2(2.0/16, 2.0/16);
        v3.uv = glm::vec2(2.0/16, 3.0/16);
        v4.uv = glm::vec2(1.0/16, 3.0/16);
        break;
    case COAL:
        v1.uv = glm::vec2(2.0/16, 2.0/16);
        v2.uv = glm::vec2(3.0/16, 2.0/16);
        v3.uv = glm::vec2(3.0/16, 3.0/16);
        v4.uv = glm::vec2(2.0/16, 3.0/16);
        break;
    case LEAF:
        v1.uv = glm::vec2(5.0/16, 3.0/16);
        v2.uv = glm::vec2(6.0/16, 3.0/16);
        v3.uv = glm::vec2(6.0/16, 4.0/16);
        v4.uv = glm::vec2(5.0/16, 4.0/16);
        break;
    case WOOD:
        v1.uv = glm::vec2(4.0/16, 1.0/16);
        v2.uv = glm::vec2(5.0/16, 1.0/16);
        v3.uv = glm::vec2(5.0/16, 2.0/16);
        v4.uv = glm::vec2(4.0/16, 2.0/16);
        break;
    case WATER:
        v1.uv = glm::vec2(13.0/16+adj, 13.0/16-adj);
        v2.uv = glm::vec2(14.0/16-adj, 13.0/16-adj);
        v3.uv = glm::vec2(13.0/16+adj, 12.0/16+adj);
        v4.uv = glm::vec2(14.0/16-adj, 12.0/16+adj);
        break;
    case LAVA:
        v1.uv = glm::vec2(13.0/16+adj, 14.0/16+adj);
        v2.uv = glm::vec2(14.0/16-adj, 14.0/16+adj);
        v3.uv = glm::vec2(14.0/16-adj, 15.0/16-adj);
        v4.uv = glm::vec2(13.0/16+adj, 15.0/16-adj);
        break;
    case GOLD:
        v1.uv = glm::vec2(0/16, 2.0/16);
        v2.uv = glm::vec2(1.0/16, 2.0/16);
        v3.uv = glm::vec2(1.0/16, 3.0/16);
        v4.uv = glm::vec2(0/16, 3.0/16);
        break;
    case DIAMOND:
        v1.uv = glm::vec2(2.0/16+adj, 3.0/16+adj);
        v2.uv = glm::vec2(3.0/16-adj, 3.0/16+adj);
        v3.uv = glm::vec2(3.0/16-adj, 4.0/16-adj);
        v4.uv = glm::vec2(2.0/16+adj, 4.0/16-adj);
        break;
    case SAND:
        v1.uv = glm::vec2(2.0/16, 1.0/16);
        v2.uv = glm::vec2(3.0/16, 1.0/16);
        v3.uv = glm::vec2(3.0/16, 2.0/16);
        v4.uv = glm::vec2(2.0/16, 2.0/16);
        break;
    case SNOW:
        v1.uv = glm::vec2(2.0/16, 4.0/16);
        v2.uv = glm::vec2(3.0/16, 4.0/16);
        v3.uv = glm::vec2(3.0/16, 5.0/16);
        v4.uv = glm::vec2(2.0/16, 5.0/16);
        break;
    }
}

void Chunk::populate_vertex_data(vector<Vertex> *v) {
    map<tuple<int, int, int>, BlockType>::iterator it;

    vector<Vertex> &vertex_data = *v;
    vertex_data.clear();
    for (auto const& cube : cubeMap) {
        tuple<int,int,int> cube_pos = cube.first;
        BlockType typ = cube.second;

        int x = get<0>(cube_pos);
        int y = get<1>(cube_pos);
        int z = get<2>(cube_pos);

        // check all 6 cardinal directions for existence of a cube
        // top
        tuple<int, int, int> top_pos = make_tuple(x, y+1, z);
        it = cubeMap.find(top_pos);
        if (it == cubeMap.end()) {
            // render this face
            Vertex v1 = Vertex();
            Vertex v2 = Vertex();
            Vertex v3 = Vertex();
            Vertex v4 = Vertex();
            v1.pos = vec4(x, y+1, z, 1);
            v1.norm = vec4(0, 1, 0, 0);
            v1.col = vec4(0, 1, 0, 1);
            v2.pos = vec4(x+1, y+1, z, 1);
            v2.norm = vec4(0, 1, 0, 0);
            v2.col = vec4(0, 1, 0, 1);
            v3.pos = vec4(x+1, y+1, z+1, 1);
            v3.norm = vec4(0, 1, 0, 0);
            v3.col = vec4(0, 1, 0, 1);
            v4.pos = vec4(x, y+1, z+1, 1);
            v4.norm = vec4(0, 1, 0, 0);
            v4.col = vec4(0, 1, 0, 1);
            set_texture(v1, v2, v3, v4, TOP, typ, true);
            vertex_data.push_back(v1);
            vertex_data.push_back(v2);
            vertex_data.push_back(v3);
            vertex_data.push_back(v4);
            face_cnt += 1;
        }
        else {
            if (cubeMap.at(top_pos) == AIR) {
                continue;
            }
        }

        // bot
        tuple<int, int, int> bot_pos = make_tuple(x, y-1, z);
        it = cubeMap.find(bot_pos);
        if (it == cubeMap.end()) {
            // render this face
            Vertex v1 = Vertex();
            Vertex v2 = Vertex();
            Vertex v3 = Vertex();
            Vertex v4 = Vertex();
            v1.pos = vec4(x, y, z, 1);
            v1.norm = vec4(0, -1, 0, 0);
            v1.col = vec4(0, 1, 0, 1);
            v2.pos = vec4(x+1, y, z, 1);
            v2.norm = vec4(0, -1, 0, 0);
            v2.col = vec4(0, 1, 0, 1);
            v3.pos = vec4(x+1, y, z+1, 1);
            v3.norm = vec4(0, -1, 0, 0);
            v3.col = vec4(0, 1, 0, 1);
            v4.pos = vec4(x, y, z+1, 1);
            v4.norm = vec4(0, -1, 0, 0);
            v4.col = vec4(0, 1, 0, 1);
            set_texture(v1, v2, v3, v4, BOT, typ, true);
            vertex_data.push_back(v1);
            vertex_data.push_back(v2);
            vertex_data.push_back(v3);
            vertex_data.push_back(v4);
            face_cnt += 1;
        }
        // front
        tuple<int, int, int> front_pos = make_tuple(x, y, z-1);
        it = cubeMap.find(front_pos);
        if (it == cubeMap.end()) {
            // render this face
            Vertex v1 = Vertex();
            Vertex v2 = Vertex();
            Vertex v3 = Vertex();
            Vertex v4 = Vertex();
            v1.pos = vec4(x, y, z, 1);
            v1.norm = vec4(0, 0, -1, 0);
            v1.col = vec4(1, 0, 0, 1);
            v2.pos = vec4(x+1, y, z, 1);
            v2.norm = vec4(0, 0, -1, 0);
            v2.col = vec4(1, 0, 0, 1);
            v3.pos = vec4(x+1, y+1, z, 1);
            v3.norm = vec4(0, 0, -1, 0);
            v3.col = vec4(1, 0, 0, 1);
            v4.pos = vec4(x, y+1, z, 1);
            v4.norm = vec4(0, 0, -1, 0);
            v4.col = vec4(1, 0, 0, 1);
            set_texture(v1, v2, v3, v4, SIDE, typ, true);
            vertex_data.push_back(v1);
            vertex_data.push_back(v2);
            vertex_data.push_back(v3);
            vertex_data.push_back(v4);
            face_cnt += 1;
        }
        // back
        tuple<int, int, int> back_pos = make_tuple(x, y, z+1);
        it = cubeMap.find(back_pos);
        if (it == cubeMap.end()) {
            // render this face
            Vertex v1 = Vertex();
            Vertex v2 = Vertex();
            Vertex v3 = Vertex();
            Vertex v4 = Vertex();
            v1.pos = vec4(x, y, z+1, 1);
            v1.norm = vec4(0, 0, 1, 0);
            v1.col = vec4(1, 0, 0, 1);
            v2.pos = vec4(x+1, y, z+1, 1);
            v2.norm = vec4(0, 0, 1, 0);
            v2.col = vec4(1, 0, 0, 1);
            v3.pos = vec4(x+1, y+1, z+1, 1);
            v3.norm = vec4(0, 0, 1, 0);
            v3.col = vec4(1, 0, 0, 1);
            v4.pos = vec4(x, y+1, z+1, 1);
            v4.norm = vec4(0, 0, 1, 0);
            v4.col = vec4(1, 0, 0, 1);
            set_texture(v1, v2, v3, v4, SIDE, typ, true);
            vertex_data.push_back(v1);
            vertex_data.push_back(v2);
            vertex_data.push_back(v3);
            vertex_data.push_back(v4);
            face_cnt += 1;
        }
        // left
        tuple<int, int, int> left_pos = make_tuple(x-1, y, z);
        it = cubeMap.find(left_pos);
        if (it == cubeMap.end()) {
            // render this face
            Vertex v1 = Vertex();
            Vertex v2 = Vertex();
            Vertex v3 = Vertex();
            Vertex v4 = Vertex();
            v1.pos = vec4(x, y, z, 1);
            v1.norm = vec4(-1, 0, 0, 0);
            v1.col = vec4(1, 0, 0, 1);
            v2.pos = vec4(x, y, z+1, 1);
            v2.norm = vec4(-1, 0, 0, 0);
            v2.col = vec4(1, 0, 0, 1);
            v3.pos = vec4(x, y+1, z+1, 1);
            v3.norm = vec4(-1, 0, 0, 0);
            v3.col = vec4(1, 0, 0, 1);
            v4.pos = vec4(x, y+1, z, 1);
            v4.norm = vec4(-1, 0, 0, 0);
            v4.col = vec4(1, 0, 0, 1);
            set_texture(v1, v2, v3, v4, SIDE, typ, true);
            vertex_data.push_back(v1);
            vertex_data.push_back(v2);
            vertex_data.push_back(v3);
            vertex_data.push_back(v4);
            face_cnt += 1;
        }
        // right
        tuple<int, int, int> right_pos = make_tuple(x+1, y, z);
        it = cubeMap.find(right_pos);
        if (it == cubeMap.end()) {
            // render this face
            Vertex v1 = Vertex();
            Vertex v2 = Vertex();
            Vertex v3 = Vertex();
            Vertex v4 = Vertex();
            v1.pos = vec4(x+1, y, z, 1);
            v1.norm = vec4(1, 0, 0, 0);
            v1.col = vec4(1, 0, 0, 1);
            v2.pos = vec4(x+1, y, z+1, 1);
            v2.norm = vec4(1, 0, 0, 0);
            v2.col = vec4(1, 0, 0, 1);
            v3.pos = vec4(x+1, y+1, z+1, 1);
            v3.norm = vec4(1, 0, 0, 0);
            v3.col = vec4(1, 0, 0, 1);
            v4.pos = vec4(x+1, y+1, z, 1);
            v4.norm = vec4(1, 0, 0, 0);
            v4.col = vec4(1, 0, 0, 1);
            set_texture(v1, v2, v3, v4, SIDE, typ, true);
            vertex_data.push_back(v1);
            vertex_data.push_back(v2);
            vertex_data.push_back(v3);
            vertex_data.push_back(v4);
            face_cnt += 1;
        }
    }
}

void Chunk::generate_attr() {
    attr_bound = true;
    context->glGenBuffers(1, &buf_attr);
}

bool Chunk::bind_attr() {
    if (attr_bound) {
        context->glBindBuffer(GL_ARRAY_BUFFER, buf_attr);
    }
    return attr_bound;
}

void Chunk::create() {
    vector<GLuint> chunk_idx = vector<GLuint>();;
    vector<Vertex> vertex_data = vector<Vertex>();
    face_cnt = 0;
    populate_vertex_data(&vertex_data);
    for (int i = 0; i < face_cnt; i++) {
        chunk_idx.push_back(i * 4);
        chunk_idx.push_back(i * 4 + 1);
        chunk_idx.push_back(i * 4 + 2);
        chunk_idx.push_back(i * 4);
        chunk_idx.push_back(i * 4 + 2);
        chunk_idx.push_back(i * 4 + 3);
    }

    count = chunk_idx.size();

    generateIdx();
    context->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufIdx);
    context->glBufferData(GL_ELEMENT_ARRAY_BUFFER, chunk_idx.size() * sizeof(GLuint), chunk_idx.data(), GL_STATIC_DRAW);

    generate_attr();
    context->glBindBuffer(GL_ARRAY_BUFFER, buf_attr);
    context->glBufferData(GL_ARRAY_BUFFER, vertex_data.size() * sizeof(Vertex), vertex_data.data(), GL_STATIC_DRAW);
}

void Chunk::addCube(std::tuple<int, int, int> pos, BlockType typ){
    cubeMap.insert(std::pair<std::tuple<int, int, int>, BlockType>(pos, typ) );
}

bool Chunk::cubeAt(float x , float y, float z)
{

    std::map<std::tuple<int, int, int>,  BlockType>::iterator iter;
    iter = cubeMap.find(std::make_tuple(floor(x), floor(y), floor(z)));

    if(iter != cubeMap.end())
    {
        BlockType bt = iter->second;
        if(bt != AIR){
            return true;
        }
    }

    iter = cubeMap.find(std::make_tuple(ceil(x), ceil(y), ceil(z)));

    if(iter != cubeMap.end())
    {
        return true;
    }
    return false;
}

BlockType Chunk::get_cube_type(int x, int y, int z) {
    return cubeMap.at(std::make_tuple(x, y, z));
}
