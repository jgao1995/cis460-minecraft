#include "plus.h"
#include <iostream>

GLenum Plus::drawMode() {
    return GL_LINES;
}


void Plus::setDimensions(glm::ivec3 d) {
    screen_height = d.x;
    screen_width = d.y;
}

void Plus::create() {
    std::vector<GLuint> idx = {0, 1, 2, 3};

    int h = context->height();
    int w =  context->width();

    //screen center
    float x = w / 2 + 10;
    float y = h / 2 + 10;

    //convert to ndc
    x = 2.0f * x / w - 1;
    y = 1 - 2.0f * y / h;

    std::vector<glm::vec4> vert_pos = {glm::vec4(-x,0,0,1), glm::vec4(x,0,0,1),

                                       glm::vec4(0,-y,0,1), glm::vec4(0,y,0,1)};

    std::vector<glm::vec4> vert_col={glm::vec4(1,1,1,1), glm::vec4(1,1,1,1),
                                     glm::vec4(1,1,1,1), glm::vec4(1,1,1,1)};

    count = 4;
    generateIdx();
    context->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufIdx);
    context->glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(GLuint), idx.data(),GL_STATIC_DRAW);

    generatePos();
    context->glBindBuffer(GL_ARRAY_BUFFER, bufPos);
    context->glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(glm::vec4), vert_pos.data(), GL_STATIC_DRAW);

    generateCol();
    context->glBindBuffer(GL_ARRAY_BUFFER, bufCol);
    context->glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(glm::vec4), vert_col.data(), GL_STATIC_DRAW);


}
