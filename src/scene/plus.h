#ifndef PLUS_H
#define PLUS_H

#include "drawable.h"
#include <la.h>

#include <QOpenGLContext>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>

class Plus : public Drawable
{
private:
    int screen_height;
    int screen_width;
public:
    Plus(GLWidget277* context) : Drawable(context){}
    void create();
    GLenum drawMode();
    void setDimensions(glm::ivec3);
};

#endif // PLUS_H
