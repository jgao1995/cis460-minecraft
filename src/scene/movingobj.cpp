#include "movingobj.h"

MovingObj::MovingObj()
{

}


MovingObj::MovingObj(int x, int y, int z): closeFlag(true), destination(glm::vec3(rand()%100, 0, rand()%100))
{
    pos = glm::vec3(x, y, z);
}


void MovingObj::move(float x, float y, float z)
{

    //    pos = glm::vec3( glm::translate(glm::mat4(1), glm::vec3(x,y,z)) * glm::vec4(pos.x, pos.y, pos.z, 1) );
    pos = glm::vec3(pos.x + x, pos.y + y, pos.z + z);
}

void MovingObj::navigate(Scene *s)
{
    float mx = 0;
    float mz = 0;
    float my = 0;

    if(destination.x - pos.x > 0.3 )
    {
        mx = MOVING_SPEED;
    } else if (destination.x - pos.x < -0.3 )
    {
        mx = -MOVING_SPEED;
    }

    if(destination.z - pos.z > 0.3 )
    {
        mz = MOVING_SPEED;
    } else if (destination.z - pos.z  < -0.3 )
    {
        mz = -MOVING_SPEED;
    }

    move(mx, 0, mz);
    int top = s->toppestPointAt(pos.x,pos.y,pos.z);
    move(0, top - pos.y, 0);

}

bool MovingObj::isClose(){
    if(glm::distance(glm::vec3(pos.x, 0, pos.z), destination) < 1){
        closeFlag = true;
    } else{
        closeFlag = false;
    }
    return closeFlag;


}

void MovingObj::setDestination(float x, float y, float z)
{
    destination = glm::vec3(x,0,z);

}
