#ifndef WEATHER_H
#define WEATHER_H

#pragma once
#include "la.h"
#include <iostream>
#include "chunk.h"

class Weather
{
private:
    BlockType type;
    glm::vec3 pos;
    float fall_speed;
public:
    Weather();
    Weather(float x, float y, float z, BlockType b);
    void fall(float speed, glm::vec3 wind_dir);
    glm::vec3 Pos();
    BlockType Type();
    glm::vec4 Color();
};

#endif // WEATHER_H
