#include "perlinnoise.h"

PerlinNoise::PerlinNoise(): p(std::vector<int>())
{
    p.resize(256);
    //Creating the permuation table
    for (int i=0; i < 256 ; i++)
    {
        p[256+i] = p[i] = permutation[i];
    }

}

PerlinNoise::PerlinNoise(unsigned int seed) : p(std::vector<int>())
{
    int indx = 0;
    p.resize(256);
    std::iota(p.begin(), p.end(), 0);
    std::default_random_engine engine(seed);
    std::shuffle(p.begin(), p.end(), engine);
    p.insert(p.end(), p.begin(), p.end());

//    qDebug()<<"hahaha no problem";

}

double PerlinNoise::fade(double t)
{
    return t * t * t * (t * (t * 6 - 15) + 10);
}

double PerlinNoise::lerp(double t, double a, double b)
{
    return a + t * (b - a);
}

double PerlinNoise::grad(int hash, double x, double y, double z)
{
    int h = hash & 15;
    double u = h<8 ? x : y,
            v = h<4 ? y : h==12||h==14 ? x : z;
    return ((h&1) == 0 ? u : -u) + ((h&2) == 0 ? v : -v);
}

//Adapt from Ken Perlin's code for improving noise
double PerlinNoise::noiseAt(double x, double y, double z){
//    return OctavePerlin(x,y,z, 8, 1/2);
    return perlin(x,y,z);
}

//Adapt from Ken Perlin's code for improving noise
double PerlinNoise::perlin(double x, double y, double z){
    int X = (int)floor(x) & 255,
            Y = (int)floor(y) & 255,
            Z = (int)floor(z) & 255;
    x -= floor(x);
    y -= floor(y);
    z -= floor(z);
    double u = fade(x),
            v = fade(y),
            w = fade(z);
    int A = p[X  ]+Y, AA = p[A]+Z, AB = p[A+1]+Z,
            B = p[X+1]+Y, BA = p[B]+Z, BB = p[B+1]+Z;

    return lerp(w, lerp(v, lerp(u, grad(p[AA  ], x  , y  , z   ),
                                grad(p[BA  ], x-1, y  , z   )),
                        lerp(u, grad(p[AB  ], x  , y-1, z   ),
                             grad(p[BB  ], x-1, y-1, z   ))),
                lerp(v, lerp(u, grad(p[AA+1], x  , y  , z-1 ),
                     grad(p[BA+1], x-1, y  , z-1 )),
            lerp(u, grad(p[AB+1], x  , y-1, z-1 ),
            grad(p[BB+1], x-1, y-1, z-1 ))));
}

double PerlinNoise::OctavePerlin(double x, double y, double z, int octaves, double persistence) {
    double total = 0;
    double frequency = 1;
    double amplitude = 1;
    double maxValue = 0;  // Used for normalizing result to 0.0 - 1.0
    for(int i=0;i<octaves;i++) {
        total += perlin(x * frequency, y * frequency, z * frequency) * amplitude;

        maxValue += amplitude;

        amplitude *= persistence;
        frequency *= 2;
    }

    return total/maxValue;
}
