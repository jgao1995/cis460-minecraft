INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

SOURCES += \
    $$PWD/main.cpp \
    $$PWD/mainwindow.cpp \
    $$PWD/glwidget277.cpp \
    $$PWD/mygl.cpp \
    $$PWD/shaderprogram.cpp \
    $$PWD/utils.cpp \
    $$PWD/la.cpp \
    $$PWD/drawable.cpp \
    $$PWD/camera.cpp \
    $$PWD/cameracontrolshelp.cpp \
    $$PWD/scene/cube.cpp \
    $$PWD/scene/scene.cpp \
    $$PWD/scene/transform.cpp \
    $$PWD/scene/perlinnoise.cpp \
    $$PWD/scene/plus.cpp \
    $$PWD/scene/chunk.cpp \
    $$PWD/scene/movingobj.cpp \
    $$PWD/scene/precipitation.cpp \
    $$PWD/scene/weather.cpp
    $$PWD/scene/plus.cpp

HEADERS += \
    $$PWD/la.h \
    $$PWD/mainwindow.h \
    $$PWD/glwidget277.h \
    $$PWD/mygl.h \
    $$PWD/shaderprogram.h \
    $$PWD/utils.h \
    $$PWD/drawable.h \
    $$PWD/camera.h \
    $$PWD/cameracontrolshelp.h \
    $$PWD/scene/cube.h \
    $$PWD/scene/scene.h \
    $$PWD/scene/transform.h \
    $$PWD/scene/perlinnoise.h \
    $$PWD/scene/plus.h \
    $$PWD/scene/chunk.h \
    $$PWD/../../Simple OpenGL Image Library/src/SOIL.h \
    $$PWD/../../Simple OpenGL Image Library/src/SOIL.h \
    $$PWD/scene/movingobj.h \
    $$PWD/scene/precipitation.h \
    $$PWD/scene/weather.h
    $$PWD/scene/plus.h



DISTFILES += \
    $$PWD/readme_suzanne.txt
